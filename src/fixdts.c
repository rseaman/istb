/* FIXDTS - setuid (dts) command to symlink the iSTB file back to DTS spool file
 * R. Seaman - 2012-06-22
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "config.h"

main (argc, argv)
int	argc;
char	*argv[];
{
	char	parfile[SZ_PATHNAME], fname[SZ_PATHNAME], host[SZ_PATHNAME];
	FILE	*fd;

	if (argc < 3) {
	    exit (-1);
	}

	umask (DEF_UMASK);
	unlink (argv[2]);
	symlink (argv[1], argv[2]);
	exit (0);
}
