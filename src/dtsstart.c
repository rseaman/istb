#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include "status.h"

main (argc, argv)
int	argc;
char	*argv[];
{
	struct queue_status	stb;

        if (! read_status (STATUS, &stb))
	    printf ("error reading status file %s\n", STATUS);

        stb.status = STAGING;

        if (! write_status (stb, STATUS))
	    printf ("error writing status file %s\n", STATUS);

	if (! enable_printing())
	    printf ("error enabling processing\n");

	if (! kick_start())
	    printf ("error kick starting\n");

	exit (0);
}
