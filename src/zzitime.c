/* ZZITIME -- convert a Unix epoch count of seconds to date and time.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "config.h"
#include "directory.h"

#define	ERR		0
#define	OK		1

main (argc, argv)
int	argc;
char	*argv[];
{
	int	itime=0;

	if (argc > 1)
	    itime = atoi (argv[1]);
	else
	    itime = now ();

	printf ("%s\n", time_string (itime));
	printf ("%s\n", utc_date_time (itime));
}
