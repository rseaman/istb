/* DTSLOGGER -- append a record to the DTS log file on an instrument computer.
 * Errors are written to the same log, or are silently discarded.
 * When called with no argument, logs contents of cwd to stdout.
 *
 * Dtslogger also generates the master observing calendar date stamp
 * propagated via front-end and network queues to dtstracker and iSTB,
 * creating a traceable datekeeping system.  These are sent to the stdout
 * to be captured by postproc and passed via the lpd Class.  A character,
 * DATE_PREFIX, is prepended because LPRng expects a priority class letter.
 *
 * Now supports post-exposure epoch timestamp.
 *
 * R. Seaman - 2007-08-14
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "config.h"
#include "directory.h"

#define	ERR		0
#define	OK		1

main (argc, argv)
int	argc;
char	*argv[];
{
	int	status=0, itime=0;

	struct	dir	*dp, *zopdir();
	char	logfile[SZ_PATHNAME];
	char	caldate[16], caldate2[16];
        char    fname[SZ_PATHNAME], host[SZ_PATHNAME];
        int     nfile, i, hlen;

	FILE	*fd;

	itime = now();
	sprintf (caldate, "%s", iso_date_string (itime - 3600*(PIVOT)));
	if (! dtm_from_short (caldate, caldate2)) {
	    if (argc <= 1)
		printf ("DTSLOGGER: date error\n");
	    exit (-1);
	}

	if (argc <= 1)
	    printf ("current local observing date is ");

	printf ("%s%s", DATE_PREFIX, caldate2);
	printf ("%s%d\n", EPOCH_PREFIX, itime);

	if (argc <= 1) {
            fd = stdout;

            if ((dp = zopdir (".")) == NULL) {
                printf ("DTSLOGGER: error opening CWD\n");
                exit (-1);
            }

            while (zgfdir (dp, fname, SZ_PATHNAME) != EOF)
                if (! dolog (fname, caldate2, itime, fd))
                    status = -1;

            zcldir (dp);

	} else {
            if (gethostname (host, SZ_PATHNAME) != 0) {
/*                printf ("DTSLOGGER: error getting hostname\n");
 */
                exit (-1);
            }

	    hlen = strlen (host);

            for (i=0; i < hlen; i++)
                if (host[i] == '.') {
                    host[i] = (char) NULL;
                    hlen = i + 1;
                    break;
                }

            if (hlen > 0)
                sprintf (logfile, "%s/%s_%s.log", DTSPATH, host, caldate);
            else
                sprintf (logfile, "%s/NO_HOST_%s.log", DTSPATH, caldate);

	    umask (DEF_UMASK);

            if ((fd = fopen (logfile, "a")) == NULL) {
/*                printf ("DTSLOGGER: error opening %s\n", logfile);
 */
                exit (-1);
            }

            for (nfile=1; nfile < argc; nfile++)
                if (! dolog (argv[nfile], caldate2, itime, fd))
                    status = -1;

            if (fflush (fd) || fsync (fileno(fd)) || fclose (fd)) {
		/* might as well try to log the error
		 */
                fprintf (fd, "DTSLOGGER: error closing %s\n", logfile);
                status = -1;
            }
	}

	exit (status);
}


int dolog (fname, caldate, itime, fpout)
char	*fname;
char	*caldate;
int	itime;
FILE	*fpout;
{
	struct	stat	sbuf;
	char	mcaldate[16], mcaldate2[16];
	int	mtime, size, uid, gid, nlink;
	unsigned mode;

	if (stat (fname, &sbuf) != 0) {
	    fprintf (fpout, "%8s %10d _NO_STAT_ %s\n", caldate, itime, fname);
	    return (ERR);

	} else {
	    size = (int) sbuf.st_size;
	    mtime = (int) sbuf.st_mtime;
	    uid = (int) sbuf.st_uid;
	    gid = (int) sbuf.st_gid;
	    mode = (unsigned) sbuf.st_mode;
	    nlink = (int) sbuf.st_nlink;

	    /* should indicate caldate/itime special case
	     */
	    sprintf (mcaldate, "%s", iso_date_string (mtime - 3600*(PIVOT)));
	    if (! dtm_from_short (mcaldate, mcaldate2)) {
		fprintf (fpout, "%10d %8s %10d %9d %07o/%d/%d/%d %s\n",
		    itime, caldate, itime, size, mode, uid, gid, nlink, fname);

		tprintf (fpout, "DTSLOGGER: date error in dolog\n");
		return (ERR);
	    }

	    fprintf (fpout, "%10d %8s %10d %9d %07o/%d/%d/%d %s\n",
		itime, mcaldate2, mtime, size, mode, uid, gid, nlink, fname);
	}

	return (OK);
}
