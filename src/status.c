/* Routines to read/write the save the bits status and requests files.
 */
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "config.h"
#include "status.h"

/* Uncomment these and relink bitf to add debug fprintf's - bitmon
 * won't link unless you define the variables in bitmon.c
 *
 * extern	int	messages;
 * extern	FILE	*mp;
 */

#define	ERR	0
#define	OK	1

char	key[SZ_PATHNAME];
char	value[SZ_PATHNAME];
char	var[SZ_PATHNAME];
int	ivalue;

/* Read the status file from disk.  All status and request files must
 * end with a time record to enforce a minimal level of handshaking.
 * Only bitf modifies the status file and so the Save the Bits archive
 * context is preserved correctly through a sequence of reads/writes
 * for the filter itself.  (The lpd daemon guarantees that only a
 * single bitf is active at any given point.)
 *
 * However, bitmon must be able to read the status at unpredictable
 * times and so a successful read can not be guaranteed (while bitf
 * is active) without imposing some locking discipline on bitf.  This
 * is not desirable since bitf shouldn't be waiting on an arbitrary
 * number of bitmon instances scattered around the mountaintop.  (Or
 * even a single privileged bitmon.)
 *
 * The resolution of this is to tightly constrain the allowable inputs
 * to read_status, returning ERR for any infractions.  The normal
 * failure mode for a status file read is for bitmon to open a partially
 * written file - it's then a race to see whether bitf finishes writing
 * each line before bitmon reads it.  In a small fraction of instances
 * bitmon reads mangled lines (typically in the final drive status
 * block), but bitf manages to write enough of the final timestamp by
 * the time bitmon gets to the end of the file to avoid a `timelast'
 * error.  The trick is to pick up the signature of those mangled lines.
 *
 * Note that each bitf status update results in a new file on disk.
 * The old status is preserved in renamed files.  If bitmon opened a
 * particular instance of the status file which was subsequently renamed
 * (or later deleted), no read error should result since files are not
 * deleted while open (and since bitf never mucks with each status file
 * after initially writing it).
 *
 * Bitmon should pole read_status in a loop waiting for an OK return
 * (or timing out) in those instances where processing can not continue
 * without retrieving the current status.  On the other hand, in instances
 * where the status info can lag (e.g., the status display loop) an ERR
 * return should just invalidate the current update.  In those instances
 * where bitmon must control the drives (e.g., swapping a tape), bitmon
 * must first stop the queue by submitting a request and waiting for
 * stb.status=STOPPED (and an inactive daemon/bitf).
 */
int read_status (statusfile, queue)
char *statusfile;
struct queue_status *queue;
{
	int	m, n, firsttime=1, timelast=0;
	FILE	*fp;

	queue->status = NONE;
	queue->recno = -1;
	queue->time = -1;

	if ((fp = fopen (statusfile, "r")) == NULL)
	    return (ERR);

	/* read the status file line by line, we don't care what order
	 * except for the status in the beginning and the timestamp at
	 * the end as long as all of the fields are there
	 */
	while (! feof (fp)) {
	    if (fscanf (fp, " %s = %s ", key, value) != 2)
		continue;

	    /* all status file integer values must be non-negative
	     */
	    if (isdigit (value[0]))
		ivalue = atoi (value);
	    else
		ivalue = -1;

	    timelast = 0;

	    if (strcmp (key, "status") == 0) {
		if (! firsttime)
		    return (ERR);
		else if (strcmp (value, "STAGING") == 0)
		    queue->status = STAGING;
		else if (strcmp (value, "STOPPED") == 0)
		    queue->status = STOPPED;
		else
		    return (ERR);

	    } else if (strcmp (key, "recno") == 0) {
		if (ivalue < 0)
		    return (ERR);
		queue->recno = ivalue;

	    } else if (strcmp (key, "time") == 0) {
		if (ivalue <= 0)
		    return (ERR);
		queue->time = ivalue;
		timelast = 1;
	    }

	    firsttime = 0;
	}

	fclose (fp);

	/* check for for missing fields
	 */
	if (queue->status == NONE || queue->time <= 0 || queue->recno < 0)
	    return (ERR);

	return (timelast);
}


/* write the runtime status into a disk file
 */
int write_status (queue, statusfile)
struct queue_status queue;
char *statusfile;
{
	char	statusfile1[SZ_PATHNAME];
	char	statusfile2[SZ_PATHNAME];
	char	statusfile3[SZ_PATHNAME];
	char	statusfile4[SZ_PATHNAME];
	FILE	*fp;

	/* keep around a few levels of old status info
	 */
	sprintf (statusfile1, "%s.1", statusfile);
	sprintf (statusfile2, "%s.2", statusfile);
	sprintf (statusfile3, "%s.3", statusfile);
	sprintf (statusfile4, "%s.4", statusfile);

	unlink (statusfile4);
	rename (statusfile3, statusfile4);
	rename (statusfile2, statusfile3);
	rename (statusfile1, statusfile2);
	rename (statusfile, statusfile1);

	if ((fp = fopen (statusfile, "w")) == NULL)
	    return (ERR);

	if (queue.status == STAGING)
	    fprintf (fp, "status             = STAGING\n");
	else if (queue.status == STOPPED)
	    fprintf (fp, "status             = STOPPED\n");
	else
	    fprintf (fp, "status             = STOPPED\n");

	fprintf (fp, "recno              = %d\n", queue.recno);

	fprintf (fp, "\n");

	fprintf (fp, "time               = %d\n", now());

	if (fflush (fp) || fsync (fileno(fp)) || fclose (fp))
	    return (ERR);
	else
	    return (OK);
}
