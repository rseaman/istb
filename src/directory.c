#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

#define DIRENT_ILLEGAL_ACCESS

#include <dirent.h>

#include "config.h"
#include "directory.h"

/* Routines for returning the contents of a directory as a sorted
 * list of filename strings - modified from iraf's os$zopdir.c.
 *
 *	dp = zopdir (directory)
 *	status = zcldir (dp)
 *	status = zgfdir (dp, outstr, maxch)
 *
 * zopdir opens the directory, reads the contents into memory, and
 * qsorts the file list.  Each call to zgfdir returns the succeeding
 * element of the list.  EOF is returned at the end of the list.
 *
 * Also includes a routine to measure the diskspace consumed by the
 * files in a directory (in units of "blocksize" bytes).  This is called
 * frequently and doesn't require the files be sorted, so no advantage
 * to layering it on zgfdir.
 *
 *	disk = dirspace (directory, prefix, blocksize, nfiles)
 *
 */

#define	DEF_SBUFLEN	8192
#define	DEF_MAXENTRIES	512

static	int _getfile();
static	int compar();
static	char *sbuf;
static	int *soff;
static	int nentries;

/* zopdir -- Open a directory file.
 */
struct dir *zopdir (directory)
char *directory;
{
	register char	*ip, *op;
	register DIR	*dir;
	int	maxentries, sbuflen, n;
	int	nchars, sbufoff;
	struct	dir *dp = NULL;

	if ((dir = opendir (directory)) == NULL) {
	    return (NULL);
	}

	nentries = 0;
	sbuflen = DEF_SBUFLEN;
	maxentries = DEF_MAXENTRIES;

	sbuf = (char *) malloc (sbuflen);
	soff = (int *) malloc (maxentries * sizeof(int));
	if (sbuf == NULL || soff == NULL)
	    goto err;

	op = sbuf;
	while ((nchars = _getfile (dir, op, SZ_PATHNAME)) != EOF) {
	    soff[nentries++] = op - sbuf;
	    op += nchars + 1;

	    if (nentries >= maxentries) {
		maxentries *= 2;
		soff = (int *) realloc (soff, maxentries * sizeof(int));
		if (soff == NULL)
		    goto err;
	    }

	    if (op + SZ_PATHNAME + 1 >= sbuf + sbuflen) {
		sbuflen *= 2;
		sbufoff = op - sbuf;
		if ((sbuf = (char *) realloc (sbuf, sbuflen)) == NULL)
		    goto err;
		op = sbuf + sbufoff;
	    }
	}

	qsort ((char *) soff, nentries, sizeof(int), compar);

	if ((soff = (int *) realloc (soff, nentries * sizeof(int))) == NULL)
	    goto err;
	if ((sbuf = (char *) realloc (sbuf, op-sbuf)) == NULL)
	    goto err;
	if ((dp = (struct dir *) malloc (sizeof (struct dir))) == NULL)
	    goto err;

	/* Set up directory descriptor. */
	dp->nentries = nentries;
	dp->sbuf = sbuf;
	dp->soff = soff;
	dp->entry = 0;

	ip = directory;
	op = dp->path;
	for (n=SZ_PATHNAME;  --n > 0 && (*op++ = *ip++);  )
	    ;

	closedir (dir);
	return (dp);

err:	if (soff)
	    free (soff);
	if (sbuf)
	    free (sbuf);
	if (dp)
	    free (dp);
	closedir (dir);
	return (NULL);
}

/* zcldir -- Close a directory file.
 */
int zcldir (dp)
struct dir *dp;
{
	free (dp->sbuf);
	free (dp->soff);
	free (dp);

	return (1);
}

/* zgfdir -- Get the next file pathname from an open directory file.
 */
int zgfdir (dp, outstr, maxch)
struct	dir *dp;
char	*outstr;
int	maxch;
{
	register int	n, nchars;
	register char	*ip, *op;

	if (dp->entry < dp->nentries) {
	    /* first prepend the directory path
	     */
	    ip = dp->path;
	    op = outstr;
	    for (n = maxch, nchars=0;  --n > 0 && (*op++ = *ip++);  )
		nchars++;

	    /* back up over the EOS
	     */
	    op--;

	    /* now append the particular filename
	     */
	    ip = dp->sbuf + dp->soff[dp->entry++];  
	    for (*op++ = '/', nchars++ ;  --n >= 0 && (*op++ = *ip++);  )
		nchars++;

	    outstr[nchars] = (char) NULL;

	    return (nchars);
	} else
	    return (EOF);
}

/* GETFILE -- Get the next file name from an open directory file.
 */
static int
_getfile (dir, outstr, maxch)
DIR	*dir;
char	*outstr;
int	maxch;
{
	register int	n;
	register struct	dirent *dirp;
	register char	*ip, *op;
	int	status;

	while ((dirp = readdir (dir)) != NULL) {
	    if ((strcmp (dirp->d_name, ".") == 0) ||
		(strcmp (dirp->d_name, "..") == 0))
		continue;

	    if (dirp->d_ino != 0) {
		n = strlen (dirp->d_name);
		status = n;
		for (ip=dirp->d_name, op=outstr;  --n >= 0;  )
		    *op++ = *ip++;
		*op = (char) NULL;
		return (status);
	    }
	}

	return (EOF);
}

/* Return the space used in a directory by files beginning with the
 * specified prefix in units of the given blocksize (blocksize is
 * measured in bytes).  The argument count is set to the number of
 * such files on output.
 */
int dirspace (directory, prefix, blocksize, count)
char	*directory;
char	*prefix;
int	blocksize;
int	*count;
{
	char	pathname[SZ_PATHNAME];
	int	dirlen, prelen=0;
	float	total;

	DIR	*dir;
	struct	dirent	*dirp;
	struct	stat	fs;

	if ((dir = opendir (directory)) == NULL)
	    return (-1);

	dirlen = strlen (directory);
	prelen = strlen (prefix);

	*count = 0;
	total = 0.0;

	/* should check the errno
	 */
	while ((dirp = readdir (dir)) != NULL) {
	    if (!strcmp(dirp->d_name,".") || !strcmp(dirp->d_name,".."))
		continue;

	    if ((prelen > 0) && (strncmp (dirp->d_name, prefix, prelen) != 0))
		continue;

	    if ((dirlen + strlen(dirp->d_name) + 1) >= SZ_PATHNAME) {
		closedir (dir);
		return (-1);
	    }

	    sprintf (pathname, "%s/%s", directory, dirp->d_name);

	    if (stat (pathname, &fs) != 0) {
		closedir (dir);
		return (-1);
	    }

	    /* round down to the nearest kilobyte
	     */
	    total += (float) fs.st_size;
	    *count += 1;
	}

	closedir (dir);

	/* Use the following if fractional blocks should be counted:
	 * return (total/blocksize + (total%blocksize ? 1 : 0));
	 */

	/* Use the following if fractional blocks should NOT be counted:
	 * return (total/blocksize);
	 */

	/* return (nint (total / ((float) blocksize)));
	 * return (total / ((float) blocksize));
	 */

	/* round up
	 */
	 return (total + (float) (blocksize - 1)) / ((float) blocksize);
}

/* COMPAR -- numeric comparision routine for qsort.
 */
static int
compar (a, b)
char	*a, *b;
{
	/* Perform a comparison of two numbers expressed as strings:
	 */
	return (atoi(&sbuf[*(int *)a]) - atoi(&sbuf[*(int *)b]));

	/* If a string comparison is preferred, use this:
	 * return (strcmp (&sbuf[*(int *)a], &sbuf[*(int *)b]));
	 */
}
