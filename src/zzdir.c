#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define	SZ_PATHNAME	161
#define	MEGABYTE	1048576
#define	KILOBYTE	1024
#define	BYTES		1

main (argc, argv)
int	argc;
char	*argv[];
{
	char	name[SZ_PATHNAME];
	char	prefix[SZ_PATHNAME];
	int	nfiles, disk;

	strncpy (name, argv[1], SZ_PATHNAME);
	strncpy (prefix, argv[2], SZ_PATHNAME);

	disk = dirspace (name, prefix, MEGABYTE, &nfiles);
	printf ("disk space for %d %s* files in %s = %d Mb\n",
	    nfiles, prefix, name, disk);

	disk = dirspace (name, prefix, KILOBYTE, &nfiles);
	printf ("disk space for %d %s* files in %s = %d Kb\n",
	    nfiles, prefix, name, disk);

	disk = dirspace (name, prefix, BYTES, &nfiles);
	printf ("disk space for %d %s* files in %s = %d bytes\n",
	    nfiles, prefix, name, disk);

	exit (0);
}
