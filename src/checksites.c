#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include "sites.h"

/* FILE	*mp = stdout;
 */
FILE	*mp;

#define	ERR	0
#define	OK	1

int	messages = 0;
int	ngotten;

main (argc, argv)
int	argc;
char	*argv[];
{
	struct	site_status sinfo;
	char	fname[SZ_PATHNAME], hostname[SZ_PATHNAME], locale[SZ_PATHNAME];
	char	site[SZ_PATHNAME], tele[SZ_PATHNAME], inst[SZ_PATHNAME];
	char	root_dir[SZ_PATHNAME], dir1_key[SZ_PATHNAME];
	char	dir2_key[SZ_PATHNAME], dir3_key[SZ_PATHNAME];
	char	alias[SZ_PATHNAME];
	int	timezone;
	int	n, na, nhost, flag=0, verbose=0, findhost=0, printhosts=0;
	int	printsites=0, printtelescopes=0, printinstruments=0;
	int	printlocale=0;
	FILE	*fp;

	strncpy (fname, SITES, SZ_PATHNAME);

	if (argc > 1) {
	    for (n=1; n < argc; n++) {
		if (argv[n][0] == '-' && strlen (argv[n]) == 2) {
		    if (argv[n][1] == 'v') {
			verbose=1;
		    } else if (argv[n][1] == 'l') {
			printlocale=1;
		    } else if (argv[n][1] == 's') {
			printsites=1;
		    } else if (argv[n][1] == 't') {
			printtelescopes=1;
		    } else if (argv[n][1] == 'i') {
			printinstruments=1;
		    } else if (argv[n][1] == 'h') {
			printhosts=1;
		    } else if (argv[n][1] == 'f') {
			if (++n >= argc) {
			    print_usage ();
			    exit (-1);
			} else {
			    strncpy (hostname, argv[n], SZ_PATHNAME);
			    findhost=1;
			    continue;
			}
		    } else {
			printf ("unknown command line flag `%s'\n", argv[n]);
			print_usage ();
			exit (-1);
		    }
		} else {
		    strncpy (fname, argv[1], SZ_PATHNAME);
		    break;
		}
	    }
	}

	flag = findhost || verbose || printsites || printtelescopes ||
	    printinstruments || printhosts || printlocale;

	if (read_sites (fname, &sinfo)) {
	    if (verbose || printlocale)
		printf ("locale = %s\n", sinfo.locale);

	    if (verbose || printsites)
		for (n=1; n < sinfo.nsites; n++) {
		    printf ("%2d %s", n, sinfo.site[n]);
		    printf (" tz=%d", sinfo.timezone[n]);
		    printf (" DMS=%s", sinfo.root_dir[n]);
		    printf ("/<%s>", sinfo.dir1_key[n]);
		    printf ("/<%s>", sinfo.dir2_key[n]);
		    printf ("/<%s>\n", sinfo.dir3_key[n]);
		}

	    if (verbose || printtelescopes)
		for (n=1; n < sinfo.ntelescopes; n++)
		    printf ("%2d %s\n", n, sinfo.telescope[n]);

	    if (verbose || printinstruments)
		for (n=1; n < sinfo.ninstruments; n++)
		    printf ("%2d %s\n", n, sinfo.instrument[n]);

	    if (verbose || printhosts)
		for (n=1; n < sinfo.nhosts; n++) {
		    for (na=0; na < sinfo.nalternates; na++) {
			if (na == 0) {
			    printf ("host[%02d].name[%d]    = %s\n",
				n, na, sinfo.host[n].name[na]);
			} else {
			    printf ("         name[%d]    = %s\n",
				na, sinfo.host[n].name[na]);
			}
		    }
		    printf ("         site       = %s\n",
			sinfo.site[sinfo.host[n].site]);
		    printf ("         timezone   = %d\n",
			sinfo.timezone[sinfo.host[n].site]);
		    printf ("         telescope  = %s\n",
			sinfo.telescope[sinfo.host[n].telescope]);
		    printf ("         alias      = %s\n",
			sinfo.telescope[sinfo.host[n].alias]);
		    printf ("         instrument = %s\n\n",
			sinfo.instrument[sinfo.host[n].instrument]);
		}

	    if (findhost) {
		nhost = get_site_info (sinfo, hostname, site, &timezone,
		    root_dir, dir1_key, dir2_key, dir3_key, tele, alias, inst);

		if (nhost > 0) {
		    printf ("%s:\n", hostname);
		    printf ("  number     = %d\n", nhost);
		    printf ("  locale     = %s\n", sinfo.locale);
		    printf ("  site       = %s\n", site);
		    printf ("  timezone   = %d\n", timezone);

		    printf ("  telescope  = %s\n", tele);
		    printf ("  alias      = %s\n", alias);
		    printf ("  instrument = %s\n", inst);

		    if (strncmp (tele, "none", 4) == 0) {
			printf ("  DMS data sorted into %s/<%s>/<%s>/<%s>\n",
			    root_dir, dir1_key, dir2_key, dir3_key);
		    } else
			printf ("  Raw data sorted by observing schedule\n");

		} else {
		    printf ("no entry for host %s\n", hostname);
		}
	    }

	    if (! flag)
		printf ("%s parses ok\n", fname);

	} else
	    printf ("error parsing %s\n", fname);

	exit (0);
}


/* print an informative message
 */
print_usage ()
{
    printf ("usage: checksites [-s,-t,-i,-h,-v,-l] [-f <host>] [<site_file>]\n");
}
