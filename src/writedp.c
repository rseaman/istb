/* WRITEDP - setuid (dts) command to symlink the iSTB file back to DTS
 * 4 arguments, pathname to cache file, spool directory, cache filename, queue name
 * R. Seaman - 2012-06-14
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "config.h"

main (argc, argv)
int	argc;
char	*argv[];
{
	char	parfile[SZ_PATHNAME], fname[SZ_PATHNAME], host[SZ_PATHNAME];
	FILE	*fd;

	if (argc < 5) {
	    exit (-1);
	}

	sprintf (parfile, "%s/%s.par", argv[2], argv[4]);

	umask (DEF_UMASK);
	chdir (argv[2]);
	symlink (argv[1], argv[3]);
	
	if ((fd = fopen (parfile, "a")) == NULL) { exit (-1); }
	fprintf (fd, "deliveryName = %s\n", argv[3]);
	if (fflush (fd) || fsync (fileno(fd)) || fclose (fd)) { exit (-1); }

	exit (0);
}
