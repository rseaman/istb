/* SUMFITS -- accumulate checksums/datasums for FITS files.
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <unistd.h>

#include "config.h"
#include "header.h"
#include "directory.h"

#define	ERR		0
#define	OK		1

/* FILE	*mp = stdout;
 */
FILE	*mp;

int	messages = 0;
int	ngotten;

main (argc, argv)
int	argc;
char	*argv[];
{
	unsigned int	sum32, datasum;

	struct  dir        *dp, *zopdir();
        char    date_obs[SZ_PATHNAME], date[SZ_PATHNAME], ut[SZ_PATHNAME];
	char	fname[SZ_PATHNAME];
	int	nflag, nfile;
	int	verbose=0, recid=0, list_only=0, lmessages=0;

/*	if (argc > 1) {
 *	    for (nflag=1; nflag <= 1 && nflag < argc; nflag++) {
 *		if (argv[nflag][0] != '-' || strlen (argv[nflag]) != 2)
 *		    break;
 *
 *		if (argv[nflag][1] == 'r') {
 *		    recid=1;
 *		} else if (argv[nflag][1] == 'v') {
 *		    verbose=1;
 *		    lmessages=2;
 *		} else if (argv[nflag][1] == 'l') {
 *		    verbose=1;
 *		    lmessages=3;
 *		} else if (argv[nflag][1] == 'h') {
 *		    list_only=1;
 *		} else {
 *		    printf ("unknown command line flag `%s'\n", argv[nflag]);
 *		    print_usage ();
 *		    exit (-1);
 *		}
 *	    }
 *	}
 *
 *	nflag = verbose || recid || list_only;
 */

	nflag = 0;

	if (argc <= nflag+1) {
	    if ((dp = zopdir (".")) == NULL) {
		printf ("error opening current working directory\n");
		exit (-1);
	    }

	    while (zgfdir (dp, fname, SZ_PATHNAME) != EOF)
		procfile (fname);

	    zcldir (dp);

	} else
	    for (nfile=nflag+1; nfile < argc; nfile++)
		procfile (argv[nfile]);

	exit (0);
}

int procfile (fname)
char	*fname;
{
	unsigned int	datasum;

	struct	fitsheader hdr, ehdr;
	char	caldate[SZ_PATHNAME];
	int	size, nheader=0, pixelerror=0, tz=TZ_DEFAULT, pivot=PIVOT;
	int	sgt60;
	FILE	*fp;

	if ((fp = fopen (fname, "r")) == NULL) {
	    printf ("error opening file %s\n", fname);
	    return (ERR);
	}

	if (read_header (fp, &hdr)) {
	    sgt60 = 0;

	    if (! getcaldate (hdr.date_obs, hdr.date, hdr.ut,
		tz, pivot, caldate, &sgt60)) {

		strcpy (caldate, "_NODATE_");
	    }

	    if (hdr.recid[0] != (char) NULL)
		printf ("# FITS %s %8s %s\n", fname, caldate, hdr.recid);
	    else
		printf ("# FITS %s %8s %s _NORECID_\n", fname, caldate);

	    if (hdr.pixelsize > 0) {
		if (! skip_pixels (fp, hdr.pixelsize, &datasum))
		    pixelerror = 1;
	    } else
		datasum = 0;

	    if (pixelerror)
		printf ("%4d DATA_ERROR %d\n", nheader, hdr.pixelsize);
	    else
		printf ("%4d %010u %d\n", nheader, datasum, hdr.pixelsize);

	    free (hdr.buf);

	    while (! at_eof (fp)) {
		pixelerror = 0;

		if (read_header (fp, &ehdr)) {
		    nheader++;

		    if (! skip_pixels (fp, ehdr.pixelsize, &datasum))
			pixelerror = 1;

		    if (pixelerror)
			printf ("%4d DATA_ERROR %d\n", nheader, ehdr.pixelsize);
		    else
			printf ("%4d %010u %d\n", nheader, datasum,
			    ehdr.pixelsize);

		    free (ehdr.buf);
		}
	    }

	    fclose (fp);

	} else {
	    printf ("# OTHER %s %8s\n", fname, iso_date_string (now()));

	    fclose (fp);
	}

	return (OK);
}

int checkfile (fp, sum32)
FILE		*fp;
unsigned int	*sum32;
{
	unsigned short	sum16;
	char	record[BLOCK*RECORD];
	int	size, recsize;

	sum16 = 0;
	*sum32 = 0;
	size = 0;

	while (! feof (fp))
	    if (recsize = fread (record, sizeof(char), BLOCK*RECORD, fp)) {
		checksum (record, recsize, &sum16, sum32);
		size += recsize;
	    }

	return (size);
}


print_usage ()
{
	printf ("usage: sumfits <file>\n");
}
