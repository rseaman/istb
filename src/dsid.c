/* DSID -- generate the DSID filename for an image from header metadata.
 *
 * The NOAO Data Set Identifier (DSID) is described in a separate document but is
 * a string of five+ tokens separated by underscores, e.g.:
 *
 *       psg_090517_201521_zri.fits.fz
 *
 * This is the SOAR telescope on cerro Pachon ("ps"), and "g" is the Goodman spectrograph.
 * The next two fields are the UTC date and time representative of this exposure.  That can
 * mean several different things depending on the instrument, and can be a timestamp either
 * after the shutter closes or before it opens, but should be relatively stable for any given
 * instrument.  This is a zero exposure raw image ("zri").  It is a FITS tile compressed file
 * (".fits.fz").  The dsid command only knows about raw data and it's behavior for pipeline
 * reduced data is unpredictable.
 *
 * R. Seaman - 2014-06-11
 */

#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "config.h"
#include "header.h"
#include "directory.h"

#define	ERR		0
#define	OK		1

FILE	*mp;

int	verbose = 0;
int	quiet = 0;
int	set_wildfire = 0;
int	wildfire = 0;
int	listonly = 0;
int	reverse = 0;
int	force = 0;
int	bad_datetime = 0;

int	messages = 0;
int	ngotten;

main (argc, argv)
int	argc;
char	*argv[];
{
	int	status = 0, file_mode = 0, farg = 1;

	struct  dir        *dp, *zopdir();
        struct  stat    sbuf;
	int     mtime, size, uid, gid, nlink;
	unsigned mode;
	char	fname[SZ_PATHNAME];
	char	arg_class[SZ_PATHNAME];

	char	caldate[16], caldate2[16];
	int	iarg, nfile, i, hlen;
	FILE	*fd;

	/* mp = fopen ("junk.messages", "a");
	 */

	farg = argc + 1;

        for (iarg = 1; iarg < argc; iarg++) {
            if (argv[iarg][0] == '-') {
                if (argv[iarg][1] == 'f') {
		    file_mode = 1;

                } else if (argv[iarg][1] == 'F') {
		    force = 1;

                } else if (argv[iarg][1] == 'v') {
		    verbose = 1;

                } else if (argv[iarg][1] == 'w') {
		    set_wildfire = 1;
		    wildfire = 1;

                } else if (argv[iarg][1] == 'q') {
		    quiet = 1;

                } else if (argv[iarg][1] == 'l') {
		    listonly = 1;
		    quiet = 0;

                } else if (argv[iarg][1] == 'r') {
		    reverse = 1;

                } else if (argv[iarg][1] == 'h' || argv[iarg][1] == 'H') {
		    print_usage ();

                } else {
		    printf ("unknown flag %s\n", argv[iarg]);
		    print_usage ();
		    exit (-1);
		}

	    } else {
		farg = iarg;
		break;
	    }
        }

	fd = stdout;

	if (file_mode >= 1) {
	    if (farg >= argc) {
		printf ("-f flag requires filenames\n");
	    }

	    for (nfile=farg; nfile < argc; nfile++) {
		if (! dodsid (argv[nfile], fd))
		    status = -1;
	    }

	} else {
	    if ((dp = zopdir (".")) == NULL) {
		printf ("error opening CWD\n");
		status =-1;
		exit (-1);
	    }

	    while (zgfdir (dp, fname, SZ_PATHNAME) != EOF) {
		if (stat (fname, &sbuf) != 0) {
		    printf ("bad stat for %s\n", fname);
		    exit (-1);
		} else {
		    /* only using the mode currently
		     */
		    size = (int) sbuf.st_size;
		    mtime = (int) sbuf.st_mtime;
		    uid = (int) sbuf.st_uid;
		    gid = (int) sbuf.st_gid;
		    nlink = (int) sbuf.st_nlink;
		    mode = (unsigned) sbuf.st_mode;
		}

		if (S_ISDIR(mode))
		    continue;

		if (! dodsid (fname, fd))
		    status = -1;
	    }

	    zcldir (dp);
	}

	exit (status);
}

int dodsid (fname, fpout)
char	*fname;
FILE	*fpout;
{
	unsigned int	datasum;

	struct	fitsheader hdr, ehdr;
	char    epochstamp[SZ_PATHNAME], dateobs[SZ_PATHNAME];
	char    date[SZ_PATHNAME], uttime[SZ_PATHNAME];
	char	rootname[SZ_PATHNAME], sbidroot[SZ_PATHNAME];
	char	dsid[SZ_PATHNAME], sbid[SZ_PATHNAME];
	char	dtacquis[SZ_PATHNAME], extname[SZ_PATHNAME];
        char    tmpdate[SZ_PATHNAME], tmptime[SZ_PATHNAME];
        char    tmptele[SZ_PATHNAME], tmpobj[SZ_PATHNAME];
        char    telename[SZ_PATHNAME], imagetype[SZ_PATHNAME];
        char    instname[SZ_PATHNAME], dtnsanam[SZ_PATHNAME];
	char	caldate[SZ_PATHNAME], caldate_short[SZ_PATHNAME];
	char	channel[SZ_PATHNAME], recid[SZ_PATHNAME];
	char	sbidkey[SZ_PATHNAME];
	int	idx, size, nheader=0, pixelerror=0, tz=TZ_DEFAULT, pivot=PIVOT;
	int	sgt60, idxdot, year, month, day, oldfits, wserial;
	double	utx;
	char	*lastslash, *firstdot, *shortname, *lastdot;
	char	*inname, *outname;
	FILE	*fpin;

	lastslash = strrchr (fname, '/');

	if (lastslash != (char) NULL) {
	    shortname = ++lastslash;
	} else {
	    shortname = fname;
	    lastslash = fname;
	}

	firstdot = strchr (shortname, '.');

	if (firstdot != (char) NULL) {
	    strcpy (extname, firstdot + 1);
	    idxdot = firstdot - lastslash;
	    strcpy (rootname, lastslash);
	    rootname[idxdot] = (char) NULL;
	} else {
	    extname[0] = (char) NULL;
	    strcpy (rootname, lastslash);
	}

	if (verbose)
	    printf ("%s (from filename)\n", rootname);

	if ((fpin = fopen (fname, "r")) == NULL) {
	    fprintf (fpout, "error opening file %s\n", fname);
	    return (ERR);
	}

	if (read_header (fpin, &hdr)) {
	    sgt60 = 0;

	    /* probably overkill
	     */
	    epochstamp[0] = (char) NULL;
	    dateobs[0] = (char) NULL;
	    date[0] = (char) NULL;
	    telename[0] = (char) NULL;
	    instname[0] = (char) NULL;
	    dtacquis[0] = (char) NULL;
	    imagetype[0] = (char) NULL;
	    dtnsanam[0] = (char) NULL;
	    sbidkey[0] = (char) NULL;
	    uttime[0] = (char) NULL;
	    channel[0] = (char) NULL;
	    recid[0] = (char) NULL;

	    /* range check the dates to the STB era
	     */
	    strcpy (epochstamp, hdr.dts_utc);
	    if (atoi (epochstamp) < 1993 || atoi (epochstamp) > 2100)
		epochstamp[0] = (char) NULL;

	    strcpy (dateobs, hdr.date_obs);
	    if (atoi (dateobs) < 1993 || atoi (dateobs) > 2100)
		dateobs[0] = (char) NULL;

	    strcpy (date, hdr.date);
	    if (atoi (date) < 1993 || atoi (date) > 2100)
		date[0] = (char) NULL;

	    strcpy (telename, hdr.dts_tele);
	    strcpy (instname, hdr.dts_inst);
	    strcpy (dtacquis, hdr.dts_host);
	    strcpy (imagetype, hdr.imagetype);
	    strcpy (dtnsanam, hdr.dts_name);
	    strcpy (sbidkey, hdr.sb_id);
	    strcpy (uttime, hdr.ut);
/*	    strcpy (channel, hdr.sb_accou);
 */
	    strcpy (recid, hdr.recid);

	    if (verbose) {
		printf ("read primary header\n");
	    }

/* fprintf (fpout, "UTC      = %s for %s (pHDU)\n", uttime, fname);
 * fprintf (fpout, "DTUTC    = %s for %s (pHDU)\n", epochstamp, fname);
 * fprintf (fpout, "DATE-OBS = %s for %s (pHDU)\n", dateobs, fname);
 * fprintf (fpout, "DATE     = %s for %s (pHDU)\n", date, fname);
 * fprintf (fpout, "DTTELESC = %s for %s (pHDU)\n", telename, fname);
 * fprintf (fpout, "imagetyp = %s for %s (pHDU)\n", imagetype, fname);
 */

	    if (hdr.pixelsize > 0) {
		if (! skip_pixels (fpin, hdr.pixelsize, &datasum))
		    pixelerror = 1;

		if (verbose) {
		    printf ("skipped pixels in primary HDU\n");
		}
	    } else {
		/* placeholder for future checksum-based functionality
		 */
		datasum = 0;
	    }

	    free (hdr.buf);

	    if (verbose) {
		printf ("freed primary HDU buffer\n");
	    }

	    if (pixelerror) {
		fprintf (fpout, "ERROR reading data records for primary HDU\n");
		return (ERR);
	    }

	    /* that is, reset this mode after each image
	     */
	    if (! set_wildfire)
		wildfire = 0;

	    /* Original header will often be in the second HDU, if fpacked
	     * (MEFs will be in the primary HDU).
	     * Leave the explicit loop here as a placeholder for reading
	     * successive headers, but block breaks after first extension.
	     */
	    while (! at_eof (fpin)) {
		if (read_header (fpin, &ehdr)) {
		    nheader++;

		    if (verbose) {
			printf ("read extension HDU\n");
		    }

		    /* defaults to individual values in primary HDU
		     * each of these has their own misbehaviors per instrument
		     */
		    if (epochstamp[0] == (char) NULL ||
			epochstamp[0] == ' ') {

			/* some DTUTC keywords clobbered during remediation
			 */
			strcpy (epochstamp, ehdr.dts_utc);
			if (atoi (epochstamp) < 1993 || atoi (epochstamp) > 2100)
			    epochstamp[0] = (char) NULL;
		    }

		    if (dateobs[0] == (char) NULL) {
			strcpy (dateobs, ehdr.date_obs);

			if (atoi (dateobs) < 1993 || atoi (dateobs) > 2100)
			    dateobs[0] = (char) NULL;
		    }

		    if (date[0] == (char) NULL) {
			strcpy (date, ehdr.date);

			if (atoi (date) < 1993 || atoi (date) > 2100)
			    date[0] = (char) NULL;
		    }

		    if (telename[0] == (char) NULL)
			strcpy (telename, ehdr.dts_tele);

		    if (instname[0] == (char) NULL)
			strcpy (instname, ehdr.dts_inst);

		    if (dtacquis[0] == (char) NULL)
			strcpy (dtacquis, ehdr.dts_host);

		    if (imagetype[0] == (char) NULL)
			strcpy (imagetype, ehdr.imagetype);

		    if (dtnsanam[0] == (char) NULL)
			strcpy (dtnsanam, ehdr.dts_name);

		    if (sbidkey[0] == (char) NULL)
			strcpy (sbidkey, ehdr.sb_id);

		    if (uttime[0] == (char) NULL)
			strcpy (uttime, ehdr.ut);

		    if (recid[0] == (char) NULL)
			strcpy (recid, ehdr.recid);

		    /* WILDFIRE appends a serial number to the OBSID/RECID
		     * use this to disambiguate files on a rapid cadence
		     */
		    wserial = -1;
		    if (recid[0] != (char) NULL) {
			lastdot = strrchr (recid, '.');
			if (lastdot != (char) NULL) {
			    if (sscanf (&lastdot[1], "%d", &wserial) != 1) {
				wserial = -1;
			    }
			}
		    }

		    /* WILDFIRE puts the IR bandpass (IHJK) in CHANNEL
		     * use this to disambiguate multiple exposures for SQIID
		     */
/*		    if (channel[0] == (char) NULL)
 *			strcpy (channel, ehdr.sb_accou);
 */

		    if (verbose) {
			printf ("CHANNEL  = %s\n", channel);
		    }

		    /* some of the WILDFIRE instruments aren't labeled by name
		     */
		    if (channel[0] != (char) NULL) {
			wildfire = 1;
			strcpy (instname, "ir_imager");
			if (verbose) {
			    printf ("WILDFIRE mode set\n");
			}
		    }

		    /* should have some reality checks, but which ones?
		     */
/*		    if (sbidkey != (char) NULL) {
 *			strcpy (sbidroot, sbidkey);
 *
 *		    } else {
 */
			strcpy (sbidroot, dtnsanam);
			firstdot = strchr (dtnsanam, '.');
			if (firstdot != (char) NULL) {
			    idxdot = firstdot - dtnsanam;
			    sbidroot[idxdot] = (char) NULL;
			}
/*		    }
 */

		    if (verbose)
			printf ("%s (from keyword)\n", sbidroot);

		    if (! reverse && strcmp (rootname, sbidroot) != 0) {
			if (! quiet)
			    printf ("keyword MISMATCH with filename\n");
		    }

/*
 * fprintf (fpout, "DATE-OBS = %s for %s (eHDU)\n", dateobs, fname);
 * fprintf (fpout, "DATE     = %s for %s (eHDU)\n", date, fname);
 * fprintf (fpout, "UTC      = %s for %s (eHDU)\n", uttime, fname);
 * fprintf (fpout, "DTACQUIS = %s for %s (eHDU)\n", dtacquis, fname);
 */
		    if (verbose) {
			fprintf (fpout, "\n");
			fprintf (fpout, "DTUTC    = %s\n", epochstamp);
			fprintf (fpout, "DTTELESC = %s\n", telename);
			fprintf (fpout, "DTINSTRU = %s\n", instname);
			fprintf (fpout, "imagetyp = %s\n", imagetype);
		    }

		    free (ehdr.buf);
		}

		break;
	    }

	    fclose (fpin);

	    /* changes here will impact different instruments dramatically
	     * heuristic seeks to globally minimize the number of SBID defaults
	     * general order is DTUTC, DATEOBS, DATE
	     */
	    tmpdate[0] = (char) NULL;
	    if (dtm_to_short (epochstamp, tmpdate) == ERR) {
		epochstamp[11] = (char) NULL;

		if (dtm_to_short (dateobs, tmpdate) == ERR) {
		    if (dtm_decode (dateobs, &year, &month, &day,
			&utx, &oldfits) == OK) {

			sprintf (tmpdate, "%04d%02d%02d", year, month, day);

		    } else if (oldfits) {
			/* ISPI DATE-OBS c. 2004 = MM/DD/YYYY
			 */
			sprintf (tmpdate, "%04d%02d%02d", year, day, month);

		    } else if (dtm_to_short (date, tmpdate) == ERR) {
			date[11] = (char) NULL;
			tmpdate[0] = (char) NULL;
		    }
		}
	    }

	    if (epochstamp[10] != 'T')
		epochstamp[11] = (char) NULL;

	    if (dateobs[10] != 'T')
		dateobs[11] = (char) NULL;

	    if (date[10] != 'T')
		date[11] = (char) NULL;

	    /* different instruments during different eras either include or
	     * don't include ISO-style times in the date keywords, etc.
	     * small changes here can have large effects
	     * general order is DTUTC, DATEOBS, UT, UTC, TIME-OBS, DATE
	     * Some instruments use other keywords like UTSHUT, but in either
	     * more diverse ways.  Usually loosely represents start of exposure
	     * (except for DTUTC).  Unix file timestamps could also be used, etc.
	     */
	    tmptime[0] = (char) NULL;
	    if (tim_to_short (&epochstamp[11], tmptime) == ERR) {
		if (tim_to_short (&dateobs[11], tmptime) == ERR) {
		    if (tim_to_short (uttime, tmptime) == ERR) {
			if (tim_to_short (&date[11], tmptime) == ERR) {
			    tmptime[0] = (char) NULL;
			}
		    }
		}
	    }

	    /* rough and statistically suspect range check, allow 24:00:00
	     */
	    if ((atoi (tmptime) > 235959 && atoi (tmptime) != 240000) || atoi (tmptime) < 0)
		tmptime[0] = (char) NULL;

	    tmptele[0] = (char) NULL;

	    sprintf (tmptele, "alt");

	    /* These assignments are generally solid for major instruments (though
	     * sometimes the computer host changed).  Multi-purpose systems like ICE,
	     * WILDFIRE, ARCON, etc., can be hard to disambiguate.
	     * This needs to be revisited when new eras of data are converted since
	     * there will be additional older instruments that are not currently
	     * expressed.
	     */

	    if (get_tel_inst_prefix (telename, instname, dtacquis, tmptele, &wildfire) != OK) {
		fprintf (fpout, "ERROR generating prefix  %s (%s, %s, %s)\n",
		    tmptele, telename, instname, dtacquis);
	    }

	    sprintf (tmpobj, "u");

	    /* This has been relatively stable, but various instruments can emit
	     * headers without an identifiable type, with user-supplied types, or
	     * with the type apparently placed in keywords like "OBJECT".
	     * Keyword search order is IMAGETYPE, OBSTYPE, IMGTYPE & OBS_TYPE, but
	     * generally only one (or none) appears in an instrument's headers.
	     * When extending to older instrumentation, should review headers for
	     * different keywords.
	     * Not clear if pattern matching would be better than explicit options.
	     */

	    if (get_observation_code (imagetype, tmpobj) != OK) {
		fprintf (fpout, "ERROR generating observation code  %s (%s)\n",
		    tmpobj, imagetype);
	    }

	    bad_datetime = 0;
	    if (tmpdate[0] == (char) NULL) {
		strcpy (tmpdate, "NONODATE");
		bad_datetime = 1;
	    }

	    if (tmptime[0] == (char) NULL) {
		strcpy (tmptime, "NOTIME");
		bad_datetime = 1;
	    }

	    sprintf (sbid, "%s.%s", sbidroot, extname);

	    if (wildfire && channel != (char) NULL) {
		if (wserial >= 0) {
		    sprintf (dsid, "%s_%06s_%06s_%sri_%s_%03d.%s", tmptele,
			&tmpdate[2], tmptime, tmpobj, channel, wserial, extname);
		} else {
		    sprintf (dsid, "%s_%06s_%06s_%sri_%s.%s", tmptele,
			&tmpdate[2], tmptime, tmpobj, channel, extname);
		}
	    } else {
		sprintf (dsid, "%s_%06s_%06s_%sri.%s",
		    tmptele, &tmpdate[2], tmptime, tmpobj, extname);
	    }

	    if (reverse) {
		inname = dsid;
		outname = sbid;
	    } else {
		inname = sbid;
		outname = dsid;
	    }

	    if (strcmp (shortname, outname) == 0) {
		if (! quiet)
		    fprintf (fpout, "skipping %s  (%s -> %s requested)\n", shortname, inname, outname);

	    } else if (! force && strcmp (shortname, inname) != 0) {
		if (! quiet)
		    fprintf (fpout, "skipping %s  (%s -> %s requested)\n", shortname, inname, outname);

	    } else {
		if (! quiet) {
		    if (bad_datetime) {
			fprintf (fpout, "bad date or time for %s  (%s -> %s requested)\n",
			    shortname, inname, outname);
		    } else {
			fprintf (fpout, "%s:  %s -> %s (%s)\n",
			    shortname, inname, outname, recid);
		    }
		}

		if (! listonly) {
		    if (access (outname, F_OK) == 0) {
			fprintf (fpout, "output file %s exists, no change to %s\n", outname, shortname);
		    } else {
			if (force || !bad_datetime || reverse) {
			    if (rename (shortname, outname) != 0)
				fprintf (fpout, "ERROR renaming %s\n", fname);
			}
		    }
		}
	    }

	} else {
	    fprintf (fpout, "skipping non-FITS file %s\n", fname);
	    fclose (fpin);
	}

	return (OK);
}

print_usage ()
{
	printf ("usage: dsid [-r] [-F] [-w] [-l] [-v] [-q] [-f <files>]\n\n");

	printf ("   -r          reverse names to sbids\n");
	printf ("   -F          force conversion\n");
	printf ("   -w          force WILDFIRE mode\n");
	printf ("   -l          list only\n");
	printf ("   -v          verbose\n");
	printf ("   -q          quiet mode\n");
	printf ("   -h          print this text\n");
	printf ("   -f <files>  list of files (rather than current directory)\n");
}
