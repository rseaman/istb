/* DSID -- generate the DSID filename for an image from header metadata.
 *
 * The NOAO Data Set Identifier (DSID) is described in a separate document but is
 * a string of five+ tokens separated by underscores, e.g.:
 *
 *       psg_090517_201521_zri.fits.fz
 *
 * This is the SOAR telescope on cerro Pachon ("ps"), and "g" is the Goodman spectrograph.
 * The next two fields are the UTC date and time representative of this exposure.  That can
 * mean several different things depending on the instrument, and can be a timestamp either
 * after the shutter closes or before it opens, but should be relatively stable for any given
 * instrument.  This is a zero exposure raw image ("zri").  It is a FITS tile compressed file
 * (".fits.fz").  The dsid command only knows about raw data and it's behavior for pipeline
 * reduced data is unpredictable.
 *
 * R. Seaman - 2014-06-11
 */

#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "config.h"

#define	ERR		0
#define	OK		1

/* GET_TEL_INST_PREFIX -- calling routine must provide an output string variable
 * long enough to contain prefixes. Since prefixes are short this is left flexible,
 * but should be revisited by - for instance - allocating a string buffer if usage
 * evolves significantly.  Also attempt to identify subset of IR instruments using
 * wildfire software.
 */

int get_tel_inst_prefix (telename, instname, hostname, prefix, wildfire)
char	*telename;
char	*instname;
char	*hostname;
char	*prefix;
int	*wildfire;
{
        char    tmptele[SZ_PATHNAME], tmpobj[SZ_PATHNAME];

	tmptele[0] = (char) NULL;

	sprintf (tmptele, "alt");

	/* These assignments are generally solid for major instruments (though
	 * sometimes the computer host changed).  Multi-purpose systems like ICE,
	 * WILDFIRE, ARCON, etc., can be hard to disambiguate.
	 * This needs to be revisited when new eras of data are converted since
	 * there will be additional older instruments that are not currently
	 * expressed.
	 */
	if (strncmp (telename, "kp09m", 6) == 0) {
	    if (strncmp (instname, "ccd_imager", 11) == 0) {
		sprintf (tmptele, "k09i");
	    } else if (strncmp (instname, "hdi", 4) == 0) {
		sprintf (tmptele, "k09h");
	    } else if (strncmp (instname, "mosaic_1", 9) == 0) {
		sprintf (tmptele, "k09m");
	    } else {
		sprintf (tmptele, "k09u");
	    }

	} else if (strncmp (telename, "kp21m", 6) == 0) {
	    if (strncmp (instname, "ccd_imager", 11) == 0 ||
		strncmp (instname, "ccd_spec", 9) == 0) {

		sprintf (tmptele, "k21i");
	    } else if (strncmp (hostname, "lapis", 6) == 0) {
		sprintf (tmptele, "k21i");
	    } else if (strncmp (instname, "ir_imager", 10) == 0) {
		*wildfire = 1;
		sprintf (tmptele, "k21w");
	    } else if (strncmp (instname, "flamingos", 10) == 0) {
		sprintf (tmptele, "k21f");
	    } else if (strncmp (instname, "gtcam", 6) == 0) {
		sprintf (tmptele, "k21g");
	    } else {
		sprintf (tmptele, "k21u");
	    }

	} else if (strncmp (telename, "kpcf", 6) == 0) {
	    sprintf (tmptele, "kcfs");

	} else if (strncmp (telename, "kp4m", 5) == 0) {
	    if (strncmp (instname, "none", 5) == 0) {
		sprintf (tmptele, "k4i");
	    } else if (strncmp (instname, "mosaic_1_1", 11) == 0) {
		sprintf (tmptele, "k4m");
	    } else if (strncmp (instname, "mosaic_1", 9) == 0) {
		sprintf (tmptele, "k4m");
	    } else if (strncmp (instname, "newfirm", 8) == 0) {
		sprintf (tmptele, "k4n");
	    } else if (strncmp (instname, "kosmos", 7) == 0) {
		sprintf (tmptele, "k4k");
	    } else if (strncmp (instname, "flamingos", 10) == 0) {
		sprintf (tmptele, "k4f");
	    /* khaki serves both ICE and Wildfire and instrument
	     * is garbled for various eras of data
	     */
	    } else if (strncmp (hostname, "khaki", 6) == 0) {
		sprintf (tmptele, "k4i");
	    } else if (strncmp (instname, "ir_imager", 10) == 0) {
		sprintf (tmptele, "k4w");
	    } else {
		sprintf (tmptele, "k4u");
	    }

	} else if (strncmp (telename, "wiyn", 5) == 0) {
	    if (strncmp (instname, "whirc", 6) == 0) {
		sprintf (tmptele, "kww");
	    } else if (strncmp (instname, "bench", 6) == 0 ||
		strncmp (instname, "hydra", 6) == 0) {

		sprintf (tmptele, "kwb");

	    } else if (strncmp (instname, "ccd_imager", 11) == 0) {
		sprintf (tmptele, "kwi");
	    } else if (strncmp (instname, "wttm", 5) == 0 ||
		strncmp (hostname, "sand", 5) == 0) {

		sprintf (tmptele, "kwi");

	    } else if (strncmp (instname, "optic", 6) == 0) {
		sprintf (tmptele, "kwo");
	    } else {
		sprintf (tmptele, "kwu");
	    }

	} else if (strncmp (telename, "soar", 5) == 0) {
	    if (strncmp (instname, "soi", 4) == 0) { 
		sprintf (tmptele, "psi");
	    } else if (strncmp (instname, "goodman", 8) == 0) {
		sprintf (tmptele, "psg");
	    } else if (strncmp (instname, "osiris", 7) == 0) {
		sprintf (tmptele, "pso");
	    } else if (strncmp (instname, "spartan", 8) == 0) {
		sprintf (tmptele, "pss");
	    } else if (strncmp (instname, "sami", 8) == 0) {
		sprintf (tmptele, "psa");
	    } else {
		sprintf (tmptele, "psu");
	    }

	} else if (strncmp (telename, "ct4m", 5) == 0) {
	    if (strncmp (instname, "ispi", 5) == 0) {
		sprintf (tmptele, "c4i");
	    } else if (strncmp (instname, "cosmos", 7) == 0) {
		sprintf (tmptele, "c4c");
	    } else if (strncmp (instname, "mosaic_2", 9) == 0) {
		sprintf (tmptele, "c4m");
	    } else if (strncmp (instname, "hydra", 5) == 0) {
		sprintf (tmptele, "c4s");
	    } else if (strncmp (instname, "ccd_spec", 9) == 0) {
		sprintf (tmptele, "c4s");
	    } else if (strncmp (instname, "decam", 6) == 0) {
		sprintf (tmptele, "c4d");
	    } else {
		sprintf (tmptele, "c4u");
	    }

	} else if (strncmp (telename, "ct09m", 6) == 0) {
	    sprintf (tmptele, "c09i");

	} else if (strncmp (telename, "ct1m", 5) == 0) {
	    sprintf (tmptele, "c1i");

	} else if (strncmp (telename, "ct13m", 6) == 0) {
	    sprintf (tmptele, "c13a");

	} else if (strncmp (telename, "ct15m", 6) == 0) {
	    if (strncmp (hostname, "ctioa2", 6) == 0) {
		sprintf (tmptele, "c15s");
	    } else if (strncmp (hostname, "ctioe1", 6) == 0) {
		sprintf (tmptele, "c15e");
	    } else {
		sprintf (tmptele, "c15s");
	    }

	} else if (strncmp (telename, "lab", 5) == 0 && strncmp (instname, "cosmos", 7) == 0) {
	    sprintf (tmptele, "clc");

	} else {
	    sprintf (tmptele, "alt");
	}

	strcpy (prefix, tmptele);

	return (OK);
}


/* GET_OBSERVATION_CODE -- calling routine must provide an output string variable
 * long enough to contain codes. Since codes are short this is left flexible, but
 * should be revisited by - for instance - allocating a string buffer if usage
 * evolves significantly. This routine assumes that something upstream (header.c)
 * has read diverse observation type keywords to populate imagetype in a fashion
 * contingent on many different instrument usage patterns.
 */
int get_observation_code (imagetype, code)
char	*imagetype;
char	*code;
{
        char    tmpobj[SZ_PATHNAME];

	sprintf (tmpobj, "u");

	/* This has been relatively stable, but various instruments can emit
	 * headers without an identifiable type, with user-supplied types, or
	 * with the type apparently placed in keywords like "OBJECT".
	 * Keyword search order is IMAGETYPE, OBSTYPE, IMGTYPE & OBS_TYPE, but
	 * generally only one (or none) appears in an instrument's headers.
	 * When extending to older instrumentation, should review headers for
	 * different keywords.
	 * Not clear if pattern matching would be better than explicit options.
	 */
	if (strncmp (imagetype, "OBJECT", 7) == 0 ||
	    strncmp (imagetype, "Object", 7) == 0 ||
	    strncmp (imagetype, "object", 7) == 0) {

	    sprintf (tmpobj, "o");

	} else if (strncmp (imagetype, "BIAS", 5) == 0 ||
	    strncmp (imagetype, "Bias", 5) == 0 ||
	    strncmp (imagetype, "bias", 5) == 0 ||
	    strncmp (imagetype, "ZERO", 5) == 0 ||
	    strncmp (imagetype, "Zero", 5) == 0 ||
	    strncmp (imagetype, "zero", 5) == 0) {

	    sprintf (tmpobj, "z");

	} else if (strncmp (imagetype, "DOME", 5) == 0 ||
	    strncmp (imagetype, "Dome", 5) == 0 ||
	    strncmp (imagetype, "dome", 5) == 0 ||
	    strncmp (imagetype, "DFLAT", 6) == 0 ||
	    strncmp (imagetype, "Dflat", 6) == 0 ||
	    strncmp (imagetype, "dflat", 6) == 0 ||
	    strncmp (imagetype, "PROJECTOR", 10) == 0 ||
	    strncmp (imagetype, "Projector", 10) == 0 ||
	    strncmp (imagetype, "projector", 10) == 0 ||
	    strncmp (imagetype, "FLAT", 5) == 0 ||
	    strncmp (imagetype, "Flat", 5) == 0 ||
	    strncmp (imagetype, "flat", 5) == 0) {

	    sprintf (tmpobj, "f");

	} else if (strncmp (imagetype, "SKY", 4) == 0 ||
	    strncmp (imagetype, "Sky", 4) == 0 ||
	    strncmp (imagetype, "sky", 4) == 0 ||
	    strncmp (imagetype, "SKYFLAT", 8) == 0 ||
	    strncmp (imagetype, "Skyflat", 8) == 0 ||
	    strncmp (imagetype, "skyflat", 8) == 0) {

	    sprintf (tmpobj, "s");

	} else if (strncmp (imagetype, "DARK", 5) == 0 ||
	    strncmp (imagetype, "Dark", 5) == 0 ||
	    strncmp (imagetype, "dark", 5) == 0) {

	    sprintf (tmpobj, "d");

	} else if (strncmp (imagetype, "CALIBRATION", 12) == 0 ||
	    strncmp (imagetype, "Calibration", 12) == 0 ||
	    strncmp (imagetype, "calibration", 12) == 0 ||
	    strncmp (imagetype, "COMP", 5) == 0 ||
	    strncmp (imagetype, "Comp", 5) == 0 ||
	    strncmp (imagetype, "comp", 5) == 0 ||
	    strncmp (imagetype, "COMPARISON", 11) == 0 ||
	    strncmp (imagetype, "Comparison", 11) == 0 ||
	    strncmp (imagetype, "comparison", 11) == 0) {

	    sprintf (tmpobj, "c");

	} else if (strncmp (imagetype, "FOCUS", 6) == 0 ||
	    strncmp (imagetype, "Focus", 6) == 0 ||
	    strncmp (imagetype, "focus", 6) == 0) {

	    sprintf (tmpobj, "g");

	} else {
	    sprintf (tmpobj, "u");
	}

	strcpy (code, tmpobj);

	return (OK);
}
