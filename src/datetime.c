#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <sys/stat.h>

#include "config.h"

extern  FILE    *mp;

#define	ERR		0
#define	OK		1

/* algorithms rely on wrapping December and January at either end
 * also ignore the Gregorian calendar change and other niceties
 */
int mlen[14] = { 31, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 31 };

char	*mname[] = {"???", "JAN", "FEB", "MAR", "APR", "MAY", "JUN",
		    "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "???"};

#define	SZ_TIMEBUF	32	/* 26 would be enough, but why quibble */

#define	DATE_ESTIMATED	"e"

char	timebuf[SZ_TIMEBUF+1];
char	*time_string();

/* TPRINTF -- Print a message to a file, prepending a timestamp.
 */
tprintf (fd, message)
FILE *fd;
char *message;
{
	fprintf (fd, "%s: %s", time_string(now()), message);
	fflush (fd);
}

/* TIMESTAMP -- Print a formatted time stamp on the specified file.
 */
timestamp (fd)
FILE *fd;
{
	fprintf (fd, "\n%s\n", time_string(now()));
}

/* TIME_STRING -- Return a pointer to a string containing the formatted
 * time for a given time in seconds.  No newline is appended.
 */
char *time_string (seconds)
int seconds;
{
	time_t	clock;

	clock = seconds;

	strncpy (timebuf, ctime(&clock), SZ_TIMEBUF);
	timebuf[SZ_TIMEBUF] = (char) NULL;

	/* remove the trailing newline */
	timebuf[strlen(timebuf)-1] = (char) NULL;

	return (timebuf);
}

/* DATE_STRING -- Return a pointer to a string containing the formatted
 * date (MMM DD YYYY, this is the same format produced by the STB date
 * stamp on KP) for a given time in seconds.  No newline is appended.
 */
char *date_string (seconds)
int seconds;
{
	struct	tm tms, *localtime();
	int	month, day, year;
	time_t	clock;

	clock = seconds;

	tms = *localtime (&clock);

	month = tms.tm_mon;
	if (month < 0 || month > 11)
	    month = 12;
	day = tms.tm_mday;
	year = tms.tm_year + 1900;

	sprintf (timebuf, "%3s %02d %04d", mname[month], day, year);
	return (timebuf);
}


/* ISO_DATE_STRING -- Return a pointer to a string containing the
 * short format ISO date (YYYYMMDD) for a given time in seconds.
 * No newline is appended.
 */
char *iso_date_string (seconds)
int seconds;
{
	struct	tm tms, *localtime();
	int	month, day, year;
	time_t	clock;

	clock = seconds;

	tms = *localtime (&clock);

	month = tms.tm_mon + 1;
	day = tms.tm_mday;
	year = tms.tm_year + 1900;

	/* assumes four digit year
	 */
	sprintf (timebuf, "%04d%02d%02d", year, month, day);
	return (timebuf);
}


/* UTC_DATE_TIME -- Return a pointer to a string containing the ISO
 * date/time (YYYY-MM-DDTHH:MM:SS) at a UTC epoch.
 * No newline is appended.  Definitely NOT multithreaded.
 */
char *utc_date_time (seconds)
int seconds;
{
	struct	tm tms, *localtime();
	int	month, day, year, hh, mm, ss;
	time_t	clock;

	clock = seconds;

	tms = *gmtime (&clock);

	/* should supposedly work through a leap second
	 */
	ss = tms.tm_sec;
	mm = tms.tm_min;
	hh = tms.tm_hour;

	month = tms.tm_mon + 1;
	day = tms.tm_mday;
	year = tms.tm_year + 1900;

	/* assumes four digit year
	 */
	sprintf (timebuf, "%04d-%02d-%02dT%02d:%02d:%02d",
	    year, month, day, hh, mm, ss);
	return (timebuf);
}


/* UTC_DATE_TIME_MS -- Return a pointer to a string containing the ISO
 * date/time (YYYY-MM-DDTHH:MM:SS.sss) at a UTC epoch.
 * No newline is appended.  Definitely NOT multithreaded.
 */
char *utc_date_time_ms (seconds, milli)
int seconds;
int milli;
{
	struct	tm tms, *localtime();
	int	month, day, year, hh, mm, ss, ms;
	time_t	clock;

	clock = seconds;

	tms = *gmtime (&clock);

	ss = tms.tm_sec;
	mm = tms.tm_min;
	hh = tms.tm_hour;

	month = tms.tm_mon + 1;
	day = tms.tm_mday;
	year = tms.tm_year + 1900;

	ms = milli;
	if (ms > 999) ms = 999;
	if (ms < 0) ms = 0;

	sprintf (timebuf, "%04d-%02d-%02dT%02d:%02d:%02d.%03d",
	    year, month, day, hh, mm, ss, ms);
	return (timebuf);
}


/* NOW -- Return the current time in seconds (from 0h UTC, 1 Jan 1970).
 */
int now ()
{
	return (time (NULL));
}

/* NOW_MS -- Return the current UTC time in seconds with milliseconds.
 */
int now_ms (milli)
int *milli;
{
	struct	timeval tvs;
	struct	timezone tzs;

	gettimeofday (&tvs, &tzs);

	*milli = (int) (tvs.tv_usec / 1000.);
	return ((int) tvs.tv_sec);
}


int getcaldate (date_obs, date, ut, timezone, pivot, caldate, sgt60)
char	*date_obs;
char	*date;
char	*ut;
int	timezone;
int	pivot;
char	*caldate;
char	*sgt60;
{
	char	datestr[SZ_PATHNAME+1];
	int	year, month, day, oldfits, ss60;
	double	utx, ltx;

	ss60 = 0;

	/* try DATE-OBS first - use the time from DATE-OBS if available
	 * otherwise use the UT keyword
	 */
	if (dtm_decode (date_obs, &year, &month, &day, &utx, &oldfits, &ss60) == OK) {
	    if (year < MINYEAR || year > MAXYEAR) { goto dlabel; }

	    if (oldfits && year < Y2KPIVOT) { year += 100; }

	    if (utx < 0.) {
		if (! sex2x (ut, &utx)) { goto dlabel; }
		if (utx < 0.0 || utx >= 24.0) { goto dlabel; }
	    }

	    if ((ltx = utx + (double) timezone) < 0) {
		ltx += 24.0;
		if (! daybefore (&year, &month, &day)) { goto dlabel; }
	    } else if (ltx >= 24.0) {
		ltx -= 24.0;
		if (! dayafter (&year, &month, &day)) { goto dlabel; }
	    }

	    if (ltx < (double) pivot) {
		if (! daybefore (&year, &month, &day)) { goto dlabel; }
	    }

	    sprintf (caldate, "%04d%02d%02d", year, month, day);
	    *sgt60 = ss60;
	    return (OK);
	}

dlabel:	if (dtm_decode (date, &year, &month, &day, &utx, &oldfits, &ss60) == OK) {
	    if (year < MINYEAR || year > MAXYEAR) { goto slabel; }

	    if (oldfits && year < Y2KPIVOT) { year += 100; }

	    if (utx < 0.) {
		if (! sex2x (ut, &utx)) { goto slabel; }
		if (utx < 0.0 || utx >= 24.0) { goto slabel; }
	    }

	    if ((ltx = utx + (double) timezone) < 0) {
		ltx += 24.0;
		if (! daybefore (&year, &month, &day)) { goto slabel; }
	    } else if (ltx >= 24.0) {
		ltx -= 24.0;
		if (! dayafter (&year, &month, &day)) { goto slabel; }
	    }

	    if (ltx < (double) pivot) {
		if (! daybefore (&year, &month, &day)) { goto slabel; }
	    }

	    sprintf (caldate, "%04d%02d%02d", year, month, day);
	    *sgt60 = ss60;
	    return (OK);
	}

	/* Should consider whether any information can be retrieved from
	 * LPRng regarding when each file was added to the original queue
	 * on the data acquisition machine.
	 */

	/* Finally fall through to the system clock on the DTS server.
	 * At this point, all we know is that the data were taken at
	 * some point before the current calendar date.  Append a suffix
         * to indicate that such dates are approximate.
	 */
slabel:	strncpy (datestr, iso_date_string (now() - 3600*(PIVOT)), SZ_PATHNAME);
	datestr[SZ_PATHNAME] = (char) NULL;

	if (datestr[0] == (char) NULL)
	    return (ERR);

/*	sprintf (caldate, "%s%s", datestr, DATE_ESTIMATED);
 */
	sprintf (caldate, "%s", datestr);
	*sgt60 = 0;
	return (OK);
}

int month_length (yyyy, mm)
int	yyyy;
int	mm;
{
	int ndays;

	if (yyyy < 1 || yyyy > MAXYEAR) { return (ERR); }
	if (mm < 1 || mm > 12) { return (ERR); }

	ndays = mlen[mm];

	/* heuristic fails on 29 February 2400
	 */
	if (mm == 2 && yyyy%4 == 0 && yyyy%100 != 0) { ndays++; }

	return (ndays);
}

int daybefore (yyyy, mm, dd)
int	*yyyy;
int	*mm;
int	*dd;
{
	int	yold, mold, dold, ynew, mnew, dnew, ndays;

	yold = *yyyy; mold = *mm; dold = *dd;

	if (yold < 1 || yold > MAXYEAR) { return (ERR); }
	if (mold < 1 || mold > 12) { return (ERR); }

	if (! (ndays = month_length (yold, mold))) { return (ERR); }

	if (dold < 1 || dold > ndays) { return (ERR); }

	ynew = yold; mnew = mold;

	if ((dnew = dold - 1) == 0) {
	    if ((mnew = mold - 1) == 0) {
	        if ((ynew = yold - 1) == 0)
		    return (ERR); /* life is too short to worry about BC */

		mnew = 12;
	    }

	    if (! (dnew = month_length (ynew, mnew))) { return (ERR); }
	}

	*yyyy = ynew;
	*mm = mnew;
	*dd = dnew;

	return (OK);
}

int dayafter (yyyy, mm, dd)
int	*yyyy;
int	*mm;
int	*dd;
{
	int	yold, mold, dold, ynew, mnew, dnew, ndays;

	yold = *yyyy; mold = *mm; dold = *dd;

	if (yold < 1 || yold > MAXYEAR) { return (ERR); }
	if (mold < 1 || mold > 12) { return (ERR); }

	if (! (ndays = month_length (yold, mold))) { return (ERR); }

	if (dold < 1 || dold > ndays) { return (ERR); }

	ynew = yold; mnew = mold;

	if ((dnew = dold + 1) > ndays) {
	    mnew += 1; dnew = 1;
	    if (mnew > 12) { ynew += 1; mnew = 1; }
	}

	*yyyy = ynew;
	*mm = mnew;
	*dd = dnew;

	return (OK);
}

/* add a number of months plus one day to a particular date
 * input date is short format, output date is long format (w/ hyphens)
 */
int expiration_date (date, nmonths)
char	*date;
int	nmonths;
{
	int	yold, mold, dold, ynew, mnew, dnew;

	if (! dtm_decode_short (date, &yold, &mold, &dold))
	    return (ERR);

	if (yold < 1 || yold > MAXYEAR) { return (ERR); }
	if (mold < 1 || mold > 12) { return (ERR); }
	if (dold < 1 || dold > month_length (yold, mold)) { return (ERR); }

	ynew = yold; mnew = mold + nmonths;
	while (mnew > 12) { ynew++; mnew -= 12; }

	/* corrects for leap days */
/*	dnew = min (dold, month_length (ynew, mnew));
 */

	if (dold > month_length (ynew, mnew))
	    dnew = month_length (ynew, mnew);
	else
	    dnew = dold;

	if (! dayafter (&ynew, &mnew, &dnew)) { return (ERR); }

	sprintf (date, "%04d-%02d-%02d", ynew, mnew, dnew);
	return (OK);
}

int sex2x (sex, x)
char	*sex;
double	*x;
{
	int	hh, mm;
	double	ss;
	char	tmp[32];
	
	strncpy (tmp, sex, 32);
	tmp[31] = (char) NULL;

	if (sscanf (tmp, "%2d:%2d:%f", &hh, &mm, &ss) != 3)
	    return (ERR);

	*x = ((double) hh) + (((double) mm) / 60.0) + (ss / 3600.0);

	return (OK);
}

/* C version of IRAF dtm_decode() function (etc$dtmcnv.x).
 * Added bounds checking on year, month, day and time values.
 * Completely reworked the algorithm.
 *
 * DTM_DECODE -- Decode the FITS format DATE-OBS string value into year,
 * month, day and time fields.  OK is returned if the date string is
 * successfully decoded, ERR if it is not.  The DATE-OBS string value
 * may be in any of the following forms:
 *   DD/MM/YY (oldfits > 0),
 *   CCYY-MM-DD (oldfits = 0, time < 0), or
 *   CCYY-MM-DDTHH:MM:SS[.SSS...] (oldfits=0, time = double precision number).
 */

int dtm_decode (datestr, year, month, day, time, oldfits, sgt60)

char	*datestr;	/*I the input date-obs string */
int	*year;		/*O the output year (< 0 if undefined) */
int	*month;		/*O the output month (< 0 if undefined) */
int	*day;		/*O the output day (< 0 if undefined) */
double	*time;		/*O the output time in hours (< 0 if undefined) */
int	*oldfits;	/*O > 0 if format matches old IRAF standard */
int	*sgt60;		/*O > 0 if seconds field > 60.0 */

{
	double	dval, t;
	float	ss;
	int	sss;
	int	y, m, d, hh, mm, old, ss60, gottime;
	char	tmp[32];

	old = 0;
	ss60 = 0;
	gottime = 0;

	strncpy (tmp, datestr, 32);
	tmp[31] = (char) NULL;

	if (sscanf (tmp, "%d-%d-%dT%d:%d:%f",
	    &y, &m, &d, &hh, &mm, &ss) == 6) {

	    /* "fix" for these legal values, e.g., during leap second
	     */
	    if (ss > 60.0) { ss60 = 1; }

	    t = (double) hh + ((double) mm / 60.) + (ss / 3600.);
	    gottime = 1;

	} else if (sscanf (tmp, "%d-%d-%d", &y, &m, &d) == 3) {
	    t = -1.;

	} else if (sscanf (tmp, "%d/%d/%d", &d, &m, &y) == 3) {
	    old = 1;
	    t = -1.;

	} else {
	    *year = -1; *month = -1; *day = -1; *time = -1.; *oldfits = 0; *sgt60=0;
	    return (ERR);
	}

	/* range checking errors will return ERR, but with values set
	 * ss > 60.0 is not an error per UTC, have to propagate through caller
	 */
	*year = y; *month = m; *day = d; *time = t; *oldfits = old; *sgt60 = ss60;

	if (y < 1) { return (ERR); }

	if (old) {
	    if (y >= 100) { return (ERR); }
	    *year += 1900;
	}

	if (m < 1 || m > 12) { return (ERR); }
	if (d < 1 || d > month_length (*year, m)) { return (ERR); }

	if (gottime && (t < 0. || t >= 24.)) { return (ERR); }

	return (OK);
}

/* version to decode terse date strings
 */
int dtm_decode_short (datestr, year, month, day)

char	*datestr;	/*I the input date-obs string */
int	*year;		/*O the output year (< 0 if undefined) */
int	*month;		/*O the output month (< 0 if undefined) */
int	*day;		/*O the output day (< 0 if undefined) */

{
	int	y, m, d;
	char	tmp[32];

	strncpy (tmp, datestr, 32);
	tmp[31] = (char) NULL;

	if (sscanf (tmp, "%4d%2d%2d", &y, &m, &d) != 3) {
	    *year = -1; *month = -1; *day = -1;
	    return (ERR);
	}

	*year = y; *month = m; *day = d;

	if (y < 1) { return (ERR); }
	if (m < 1 || m > 12) { return (ERR); }
	if (d < 1 || d > month_length (y, m)) { return (ERR); }

	return (OK);
}

/* convert yyyy-mm-dd format to yyyymmdd
 */
int dtm_to_short (datestr1, datestr2)

char	*datestr1;	/*I the input date string in ISO yyyy-mm-dd format */
char	*datestr2;	/*O the output date string in yyyymmdd format */

{
	int	y, m, d;
	char	tmp[32];

	strncpy (tmp, datestr1, 32);
	tmp[31] = (char) NULL;

	if (sscanf (tmp, "%4d-%2d-%2d", &y, &m, &d) != 3)
	    return (ERR);

	if (y < 1) { return (ERR); }
	if (m < 1 || m > 12) { return (ERR); }
	if (d < 1 || d > month_length (y, m)) { return (ERR); }

	sprintf (datestr2, "%04d%02d%02d", y, m, d);

	return (OK);
}

/* convert hh:mm:ss format to hhmmss
 */
int tim_to_short (timestr1, timestr2)

char	*timestr1;	/*I the input time string in ISO hh:mm:ss format */
char	*timestr2;	/*O the output time string in hhmmss format */

{
	int	h, m, s;
	char	tmp[32];

	if (timestr1[0] == (char) NULL || timestr1[0] == ' ')
	    return (ERR);

	strncpy (tmp, timestr1, 32);
	tmp[31] = (char) NULL;

	if (sscanf (tmp, "%2d:%2d:%2d", &h, &m, &s) != 3)
	    return (ERR);

	if (h < 0) { return (ERR); }
	if (m < 0 || 0 > 60) { return (ERR); }
	if (s < 0 || s > 61) { return (ERR); }

	sprintf (timestr2, "%02d%02d%02d", h, m, s);

	return (OK);
}

/* convert yyyymmdd format to yyyy-mm-dd
 */
int dtm_from_short (datestr2, datestr1)

char	*datestr2;	/*I the input date string in yyyymmdd format */
char	*datestr1;	/*O the output date string in ISO yyyy-mm-dd format */

{
	int	y, m, d;
	char	tmp[32];

	strncpy (tmp, datestr2, 32);
	tmp[31] = (char) NULL;

	if (sscanf (tmp, "%4d%2d%2d", &y, &m, &d) != 3)
	    return (ERR);

	if (y < 1) { return (ERR); }
	if (m < 1 || m > 12) { return (ERR); }
	if (d < 1 || d > month_length (y, m)) { return (ERR); }

	sprintf (datestr1, "%04d-%02d-%02d", y, m, d);

	return (OK);
}
