#ifndef	SZ_PATHNAME
#define	SZ_PATHNAME	161
#endif

struct dir {
	int	nentries;
	int	entry;
	char	*sbuf;
	int	*soff;
	char	path[SZ_PATHNAME];
};
