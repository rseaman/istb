#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "config.h"
#include "sites.h"

#define	ERR		0
#define	OK		1

int read_sites (sitesfile, sinfo)
char *sitesfile;
struct site_status *sinfo;
{
	char	key[SZ_PATHNAME], value[SZ_PATHNAME], var[SZ_PATHNAME];
	int	n, na, ivalue, firsttime=1;
	int	gotnsites=0, gotntelescopes=0, gotninstruments=0, gotnhosts=0;
	int	gotnalternates=0, gotlocale=0;
	FILE	*fp;

	if ((fp = fopen (sitesfile, "r")) == NULL)
	    return (ERR);

	strcpy (sinfo->locale, "NONE");

	for (n=0; n < MAX_NSITES; n++) {
	    sinfo->timezone[n] = 0;
	    strcpy (sinfo->site[n], "NONE");
	    strcpy (sinfo->root_dir[n], "NONE");
	    strcpy (sinfo->dir1_key[n], "NONE");
	    strcpy (sinfo->dir2_key[n], "NONE");
	    strcpy (sinfo->dir3_key[n], "NONE");
	}

	for (n=0; n < MAX_NTELESCOPES; n++)
	    strcpy (sinfo->telescope[n], "NONE");

	for (n=0; n < MAX_NINSTRUMENTS; n++)
	    strcpy (sinfo->instrument[n], "NONE");

	for (n=0; n < MAX_NHOSTS; n++) {
	    sinfo->host[n].alias = 0;
	    for (na=0; na < MAX_NALT; na++)
		strcpy (sinfo->host[n].name[na], "NONE");
	}

	/* Silently skip any lines not of the format <string> = <string>
	 * including blank lines & comments - if NO COMMENT INCLUDES AN "=".
	 * The file must define nsites, ntelescopes, ninstruments, nhosts,
	 * nalternates and istb.locale first.
	 * Always run checksite after editing the stb_sites file.
	 */
	while (! feof (fp)) {
	    if (fscanf (fp, " %s = %[^\n] ", key, value) != 2)
		continue;

	    if ( isdigit (value[0]) ||
		(isdigit (value[1]) && (value[0] == '-' || value[0] == '+'))) {

		ivalue = atoi (value);

	    } else
		ivalue = -1;

	    if (strcmp (key, "nsites") == 0) {
		if (! firsttime || ivalue <= 0 || ivalue > MAX_NSITES)
		    return (ERR);
		sinfo->nsites = ivalue;
		gotnsites = 1;

            } else if (strcmp (key, "ntelescopes") == 0) {
		if (ivalue <= 0 || ivalue > MAX_NTELESCOPES)
		    return (ERR);
                sinfo->ntelescopes = ivalue;
		gotntelescopes = 1;

            } else if (strcmp (key, "ninstruments") == 0) {
		if (ivalue <= 0 || ivalue > MAX_NINSTRUMENTS)
		    return (ERR);
                sinfo->ninstruments = ivalue;
		gotninstruments = 1;

            } else if (strcmp (key, "nhosts") == 0) {
		if (ivalue <= 0 || ivalue > MAX_NHOSTS)
		    return (ERR);
                sinfo->nhosts = ivalue;
		gotnhosts = 1;

            } else if (strcmp (key, "nalternates") == 0) {
		if (ivalue <= 0 || ivalue > MAX_NALT)
		    return (ERR);
                sinfo->nalternates = ivalue;
		gotnalternates = 1;

            } else if (strcmp (key, "istb.locale") == 0) {
		strncpy (sinfo->locale, value, SZ_SITES);
		gotlocale = 1;

	    } else {
		if (! gotnsites || ! gotntelescopes || ! gotninstruments ||
		    ! gotnhosts || ! gotnalternates || ! gotlocale) {

		    return (ERR);
		}

                for (n=0; n < sinfo->nsites; n++) {
		    sprintf (var, "site[%d].timezone", n);
		    if (strcmp (key, var) == 0) {
			sinfo->timezone[n] = ivalue;
			goto got1;
		    }
                }

                for (n=0; n < sinfo->nsites; n++) {
		    sprintf (var, "site[%d].name", n);
		    if (strcmp (key, var) == 0) {
			strncpy (sinfo->site[n], value, SZ_SITES);
			goto got1;
		    }
                }

                for (n=0; n < sinfo->nsites; n++) {
		    sprintf (var, "site[%d].root_dir", n);
		    if (strcmp (key, var) == 0) {
			strncpy (sinfo->root_dir[n], value, SZ_SITES);
			goto got1;
		    }
                }

                for (n=0; n < sinfo->nsites; n++) {
		    sprintf (var, "site[%d].dir1_key", n);
		    if (strcmp (key, var) == 0) {
			strncpy (sinfo->dir1_key[n], value, SZ_SITES);
			goto got1;
		    }
                }

                for (n=0; n < sinfo->nsites; n++) {
		    sprintf (var, "site[%d].dir2_key", n);
		    if (strcmp (key, var) == 0) {
			strncpy (sinfo->dir2_key[n], value, SZ_SITES);
			goto got1;
		    }
                }

                for (n=0; n < sinfo->nsites; n++) {
		    sprintf (var, "site[%d].dir3_key", n);
		    if (strcmp (key, var) == 0) {
			strncpy (sinfo->dir3_key[n], value, SZ_SITES);
			goto got1;
		    }
                }

                for (n=0; n < sinfo->ntelescopes; n++) {
		    sprintf (var, "telescope[%d].name", n);
		    if (strcmp (key, var) == 0) {
			strncpy (sinfo->telescope[n], value, SZ_SITES);
			goto got1;
		    }
                }

                for (n=0; n < sinfo->ninstruments; n++) {
		    sprintf (var, "instrument[%d].name", n);
		    if (strcmp (key, var) == 0) {
			strncpy (sinfo->instrument[n], value, SZ_SITES);
			goto got1;
		    }
                }

                for (n=0; n < sinfo->nhosts; n++) {
		    for (na=0; na < sinfo->nalternates; na++) {
			sprintf (var, "host[%d].name[%d]", n, na);
			if (strcmp (key, var) == 0) {
			    strncpy (sinfo->host[n].name[na], value,
				SZ_SITES);
			    goto got1;
			}
		    }

		    sprintf (var, "host[%d].site", n);
		    if (strcmp (key, var) == 0) {
			if (ivalue < 0 || ivalue > sinfo->nsites)
			    return (ERR);
			sinfo->host[n].site = ivalue;
			goto got1;
		    }

		    sprintf (var, "host[%d].telescope", n);
		    if (strcmp (key, var) == 0) {
			if (ivalue < 0 || ivalue > sinfo->ntelescopes)
			    return (ERR);
			sinfo->host[n].telescope = ivalue;
			goto got1;
		    }

/*		    sinfo->host[n].alias = 0;
 */
		    sprintf (var, "host[%d].alias", n);
		    if (strcmp (key, var) == 0) {
			if (ivalue < 0 || ivalue > sinfo->ntelescopes)
			    return (ERR);
			sinfo->host[n].alias = ivalue;

/* printf ("got alias[%d] = %d\n",n, ivalue);
 * printf ("sinfo->host[%d].alias = %d\n",n, sinfo->host[n].alias);
 */
			goto got1;
		    }

		    sprintf (var, "host[%d].instrument", n);
		    if (strcmp (key, var) == 0) {
			if (ivalue < 0 || ivalue > sinfo->ninstruments)
			    return (ERR);
			sinfo->host[n].instrument = ivalue;
			goto got1;
		    }
                }
	    }

got1:	    firsttime = 0;
	}

	fclose (fp);
	return (OK);
}

int get_site_info (sinfo, host, site, timezone,
root_dir, dir1_key, dir2_key, dir3_key,
telescope, alias, instrument)

struct site_status sinfo;
char *host;

char *site;
int  *timezone;

char *root_dir;
char *dir1_key;
char *dir2_key;
char *dir3_key;

char *telescope;
char *alias;
char *instrument;
{
	char	instname[SZ_SITES];
	int	n, na, site_id;

	for (n=0; n < sinfo.nhosts; n++) {
	    for (na=0; na < sinfo.nalternates; na++) {
		if (strcmp (host, sinfo.host[n].name[na]) == 0) {

		    site_id = sinfo.host[n].site;

		    *timezone = sinfo.timezone[site_id];
		    strncpy (site, sinfo.site[site_id], SZ_SITES);
		    strncpy (root_dir, sinfo.root_dir[site_id], SZ_SITES);
		    strncpy (dir1_key, sinfo.dir1_key[site_id], SZ_SITES);
		    strncpy (dir2_key, sinfo.dir2_key[site_id], SZ_SITES);
		    strncpy (dir3_key, sinfo.dir3_key[site_id], SZ_SITES);

		    strncpy (telescope,
			sinfo.telescope[sinfo.host[n].telescope], SZ_SITES);

		    strncpy (alias, "none", SZ_SITES);
/* printf ("sinfo.host[%d].alias = %d\n", n, sinfo.host[n].alias);
 */
		    if (sinfo.host[n].alias > 0) {
			strncpy (alias, sinfo.telescope[sinfo.host[n].alias], SZ_SITES);
/* printf ("alias[%d] = %s (%d)\n", n, alias, sinfo.host[n].alias);
  */
		    }

		    strncpy (instname,
			sinfo.instrument[sinfo.host[n].instrument], SZ_SITES);

		    if (strcmp (instname, "NONE") == 0)
			strncpy (instrument, "", SZ_SITES);
		    else
			strncpy (instrument, instname, SZ_SITES);

		    return (n);
		}
	    }
	}

	return (-1);
}
