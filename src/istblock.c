#include <stdio.h>
#include <sys/file.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>
#include <unistd.h>

#define	LOCKNAME	"/tmp/istb.lock"
#define	LOCKSEM 	"/tmp/istb.sem"
#define	LOCKSWITCH	"noistblock"

#define	SUCCESS		0
#define	FAILURE		-1
#define	TIMEOUT		300		/* seconds */

/* Set to 0 with adb to disable locking. */
int	istblock_enable = 1;

#ifdef	TEST
main (argc, argv)
int	argc;
char	*argv[];
{
	if (argc > 1) {
	    printf ("istbunlock = %d\n", istbunlock());
	} else {
	    printf ("istblock = %d\n", istblock());
	    sleep (TIMEOUT/2);
	    printf ("istblock = %d\n", istblock());
	    sleep (TIMEOUT*2);
	    printf ("istbunlock = %d\n", istbunlock());
	}
}
#endif

/* istblock -- try to get or update istb lock and return 0 if successful
 */
istblock ()
{
	char	*getenv ();
	int	owner, pid = getpid();
	FILE	*fp, *sp;
	struct	stat	status;
	time_t	now;

	if (!istblock_enable || getenv (LOCKSWITCH))
	    return (SUCCESS);		/* disable */

	/* The whole shebang should be rewritten to be threadsafe, failing
	 * that this locking scheme is well tested, but kind of hokey - just
	 * shrink the race conditions with a persistent semaphor and allow
	 * it to block here, not in the scary main program,
	 * closing the file releases the semaphor,
	 * holding the flock from the shell halts DTS
	 */
	sp = fopen (LOCKSEM, "w");
	flock (fileno(sp), LOCK_EX);

/* fprintf (stderr, "before sleep\n");
 * sleep (10);
 * fprintf (stderr, " after sleep\n");
 */

	/* assume failure to open means it doesn't exist
	 */
	if (fp = fopen (LOCKNAME, "r")) {
	    if (fscanf (fp, "%d", &owner) != 1) {
		fclose (fp);
		fprintf (stderr, "1 - could not get lock `%s'\n", LOCKNAME);
		fclose (sp);
		return (FAILURE);	/* could not determine ownership */
	    }
	    fclose (fp);
	    if (pid != owner) {
		errno = 0;
		getpriority (PRIO_PROCESS, owner);
		if (errno != ESRCH) {		/* process is still there */
		    if (stat (LOCKNAME, &status)) {
			fprintf (stderr, "2 - could not get lock `%s'\n", LOCKNAME);
			fclose (sp);
			return (owner); /* could not determine ownership */
		    }
		    time (&now);
		    if ((now - status.st_mtime) < TIMEOUT) {
			fprintf (stderr, "3 - could not get lock `%s', ", LOCKNAME);
			fprintf (stderr, "owned by pid %d\n", owner);
			fclose (sp);
			return (owner);	/* owned by another pid */
		    }
		}
		/* stale */
		if (unlink (LOCKNAME)) {
		    fprintf (stderr, "6 - could not unlink to lock `%s', ", LOCKNAME);
		    fprintf (stderr, "owned by pid %d, errno %d\n", owner, errno);
		    fclose (sp);
		    return (owner);
		}

		fprintf (stderr, "5 - removed stale lock `%s', ", LOCKNAME);
		fprintf (stderr, "owned by pid %d\n", owner);
	    }
	}

	if (fp = fopen (LOCKNAME, "w")) {
	    fprintf (fp, "%15d\n", pid);
            if (fflush (fp) || fsync (fileno(fp)) || fclose (fp)) {
		fprintf (stderr, "9 - error closing lock `%s'\n", LOCKNAME);
		fclose (sp);
		return (FAILURE);
	    }

	} else {
	    fprintf (stderr, "4 - could not get lock `%s'\n", LOCKNAME);
	    fclose (sp);
	    return (FAILURE);	/* couldn't create the lock */
	}

	fclose (sp);
	return (SUCCESS);	/* got an update lock */
}

/* istbunlock -- unlock istb lock and return 0 if successful
 */
istbunlock ()
{
	char	*getenv ();

	if (!istblock_enable || getenv (LOCKSWITCH))
	    return (SUCCESS);		/* disable */

	if (istblock() != SUCCESS) {
	    fprintf (stderr, "7 - could not update lock to unlock `%s'\n", LOCKNAME);
	    return (FAILURE);
	}

	/* return (unlink (LOCKNAME) ? FAILURE : SUCCESS);
	 */
	if (unlink (LOCKNAME)) {
	    fprintf (stderr, "8 - could not unlink to unlock `%s'\n", LOCKNAME);
	    fprintf (stderr, "errno %d\n", errno);
	    return (FAILURE);
	}

	return (SUCCESS);
}
