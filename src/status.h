#ifndef	SZ_PATHNAME
#define	SZ_PATHNAME	161
#endif

/* codes for queue_status.status
 */
#define	NONE		-1
#define	STAGING		0
#define	STOPPED		1

struct	queue_status {
	int	status;
	int	recno;
	int	time;
};
