/* STBLOG -- append a record to the save the bits archive log file
 * in the /tmp directory of a particular acquisition computer.
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"

char	*time_string();

main (argc, argv)
int     argc;
char    *argv[];
{
	char	*logfile="/tmp/.stblog";
	char	*image, *recid;
	FILE	*fd;
	int	len;

        if (argc != 3) {
            print_usage ();
            exit (-1);
        }

	image = argv[1];
	recid = argv[2];

	if ((fd = fopen (logfile, "a")) == NULL) {
	    printf ("error opening %s\n", logfile);
            exit (-1);
	}
	    
	fprintf (fd, "%-25s %-25s %-s\n", time_string(now()), recid, image);
	fclose (fd);
}


/* print an informative message
 */
print_usage ()
{
        printf ("usage: stblog <image> <recid>\n");
}
