procedure spacechecker (dtcaldat)

string	dtcaldat 		{prompt="Observing calendar date"}

int	bitpix = 0		{prompt="Specify bitpix (or 0) for check"}
string	fzimages = ""		{prompt="Override archive image list"}

string	site = "kpno"		{prompt="Observatory"}
string	archive_root = "/noaocache/mtn"	{prompt="Root for archive"}
string	stats_root = "/bits/stats"	{prompt="Root for compression statistics"}

struct	*list1, *list2, *list3

begin
	string	images, fname1, img, img1, img2, img3
	string	j1, j2, j3, tmpfile2, obsid1, obsid2
	string	cksum_stat, trackfile0, statsfile

	string	obsid0, dtcaldat0
	string	sb_host, sb_dir1, sb_dir2, sb_dir3, tmpval
	string	recno, dtnsanam, sb_recno, sb_id, sb_name
	string	sb_host_short, timestr
	struct	datestr

	int	ij, len, slash, quote, filcount, zbitpix
	int	starttime, endtime, runtime, nskip
	int	fitsrec, fzrec, bytesz
	real	fitssz, fzsz, gzrec, gzsz, gzratio, saved, compratio, r
	real	rfz, rgz, diffsz
	struct	line1

	cache sections

	nskip = 0
	fitssz = 0
	fzsz = 0
	gzsz = 0

	tmpfile2 = mktemp ("tmp$dts_")
	img2 = ""
	img3 = ""

	dtcaldat0 = dtcaldat

	statsfile = stats_root // "/" // site // bitpix // "_" // dtcaldat0 // ".stats"

	if (strlen (fzimages) > 0) {
	    images = fzimages
	    sections (images, opt="fullname", > tmpfile2)

	} else {
	    # would need to update this for DMS mode
	    printf ("'%s/%s/*/*/*.fz'\n", archive_root, dtcaldat0) | scan (images)
	    printf ("!ls %s\n", images) | cl (> tmpfile2)
	}

#	sections (images, opt="fullname", > tmpfile2)
#	if (sections.nimages <= 0) {
#	    printf ("no images found: %s\n", images)
#	    goto cleanup
#	}

	time | scan (datestr)
	printf ("\nSTART: %s\n\n", datestr)
	ij = fscan (datestr, j1, timestr)
	starttime = int (3600. * (real(timestr) + 0.5))

	filcount = 0

	list3 = tmpfile2
	while (fscan (list3, img) != EOF) {
	    # limit check to a specific bitpix
	    if (bitpix != 0) {
		printf ("!/bits/bin/listhead %s | grep ZBITPIX\n", img) |
		    cl | scan (j1, j2, zbitpix)

		# fragile - won't work for MEFs or with input errors
		if (zbitpix != bitpix) {
		    printf ("    skip ZBITPIX=%d (%s)\n", zbitpix, img)
		    nskip += 1
		    next
		}
	    }

	    slash = strldx ("/", img)
	    len = strlen (img)
	    printf ("/tmp/%s\n", substr(img,slash+1,len)) | scan (img1)
	    len = strlen (img1)
	    img2 = substr (img1, 1, len-3)
	    img3 = img2 // ".gz"

	    # if (access(img2)) ...
#	    printf ("!/bits/bin/funpack -S %s > %s\n", img, img2) | cl
	    printf ("!/bits/bin/funpack -O %s %s\n", img2, img) | cl

	    dir (img, long+) | scan (line1)
	    if (fscan (line1, j1, j2, bytesz) != 3) {
		printf ("error reading file size for %s\n", img)
		goto cleanup
	    } else if (bytesz <= 0) {
		printf ("file size error (%d) for %s\n", bytesz, img)
		goto cleanup
	    } else if ((bytesz % 2880) != 0) {
		printf ("size (%d) not multiple of 2880 for %s\n", bytesz, img)
		goto cleanup
	    }

	    fzrec = bytesz / 2880
	    fzsz += real (bytesz) / 1048576.

	    dir (img2, long+) | scan (line1)
	    if (fscan (line1, j1, j2, bytesz) != 3) {
		printf ("error reading file size for %s\n", img2)
		goto cleanup
	    } else if (bytesz <= 0) {
		printf ("file size error (%d) for %s\n", bytesz, img2)
		goto cleanup
	    } else if ((bytesz % 2880) != 0) {
		printf ("size (%d) not multiple of 2880 for %s\n", bytesz, img2)
		goto cleanup
	    }

	    fitsrec = bytesz / 2880
	    fitssz += real (bytesz) / 1048576.

	    compratio = real (fitsrec) / real (fzrec)

	    # creates img3
	    printf ("!gzip %s\n", img2) | cl

	    dir (img3, long+) | scan (line1)
	    if (fscan (line1, j1, j2, bytesz) != 3) {
		printf ("error reading file size for %s\n", img3)
		goto cleanup
	    } else if (bytesz <= 0) {
		printf ("file size error (%d) for %s\n", bytesz, img3)
		goto cleanup
	    }

	    gzrec = bytesz / 2880
	    gzsz += real (bytesz) / 1048576.

	    gzratio = real (fitsrec) / real (gzrec)

	    saved = 100. - 100. * (real(fzrec) / gzrec)

	    printf ("%7.3f %7.3f\n", compratio, saved, >> statsfile)

	    delete (img2, verify-, >& "dev$null")
	    delete (img3, verify-, >& "dev$null")

	    filcount += 1

	    printf ("%3d %s R(fz)=%4.2f R(gz)=%4.2f saved=%d%%\n",
		filcount, img, compratio, gzratio, saved)
	}

cleanup:
	list1 = ""; list2 = ""; list3 = ""
	delete (img2, verify-, >& "dev$null")
	delete (img3, verify-, >& "dev$null")
	delete (tmpfile2, verify-, >& "dev$null")

	if (filcount > 0) {
	    if (bitpix != 0) {
		printf ("\nCompression stats for %s [BITPIX=%d]\n", dtcaldat0, bitpix)
	    } else
		printf ("\nCompression stats for %s\n", dtcaldat0)

	    rfz = fitssz / fzsz
	    rgz = fitssz / gzsz
	    saved = 100. - 100. * (real(fzsz) / gzsz)
	    diffsz = gzsz - fzsz

	    printf ("%d files", filcount)
	    if (nskip > 0) printf (" (skipped %d)", nskip)

	    printf (", R(fz)=%4.2f (%5.2f MB -> %4.2f MB), R(gz)=%4.2f, SAVED=%6.2f MB (%d%%)\n",
		rfz, fitssz, fzsz, rgz, diffsz, saved)
	}

	time | scan (datestr)
	ij = fscan (datestr, j1, timestr)
	endtime = int (3600. * (real(timestr) + 0.5))
	runtime = endtime - starttime

	if (runtime < 0) {
	    runtime += 86400
	    if (runtime < 0) printf ("error with runtime\n")
	}

	printf ("runtime=%4.0m (%4.2fs/file, %5.3fs/MB)\n",
	    real(runtime)/60., real(runtime)/filcount, real(runtime)/fzsz)

	# data type   files  fz(MB) R(avg) uncompressed  R(gz)  space-saved
	#  16-bits    1835   5097.7  2.75  14030.5 MB
	#  32-bits     316   1548.8  3.23   5006.0 MB
	#  FP (raw)    775   1229.4  2.53   3110.4 MB

	if (bitpix == 0) {
	} else {
	    printf ("\n# site  bitpix files  fz(MB) R(avg) uncompressed  R(gz)   space-saved time-saved\n")
	    printf ("  %4s    %d   %4d   %6.1f  %4.2f  %7.1f MB    %4.2f  %6.1f MB (%d%%)  %5.0m\n",
		site, bitpix, filcount, fzsz, rfz, fitssz, rgz, diffsz, saved, real(runtime)/60.)
	}

	printf ("\nEND %s\n", datestr)
end
